#!/bin/bash
text=$*
echo $text
export DJANGO_SETTINGS_MODULE=laws_online.settings.$text
nohup snowflake_start_server --address=localhost --port=8910 --dc=1 --worker=1 > /dev/null 2> /dev/null &
python3 manage.py migrate --settings=laws_online.settings.$text
python3 manage.py collectstatic --settings=laws_online.settings.$text --noinput

#sleep 1
#export C_FORCE_ROOT=True
#nohup celery --app=laws_online worker -l INFO  >/app/laws_online/laws_online/logs/celery_worker.log 2>&1 &

#python3 manage.py rebuild_index --noinput
#python3 manage.py runserver 0.0.0.0:8000 --settings=laws_online.settings.$text
gunicorn -c gunicorn.conf.py --env DJANGO_SETTINGS_MODULE=laws_online.settings.$text laws_online.wsgi:application
#nohup python3 manage.py runserver 0:8000  > run.log 2>&1 &
#nohup celery worker -A matrix -l info  >celery.log 2>&1 &
#nohup celery -A matrix beat -l info  >celery_beat.log 2>&1 &
