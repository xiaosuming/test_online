import logging
import logging.handlers
from logging.handlers import WatchedFileHandler
import os
import multiprocessing
from pathlib import Path

BASE_DIR = Path(__file__).resolve(strict=True).parent
chdir = str(BASE_DIR)

bind = "0.0.0.0:8000"   #绑定的ip与端口
backlog = 2048                #监听队列数量，64-2048
# chdir = '=/home/iwgp-xhjs/s/shimadzuKnows'  #gunicorn要切换到的目的工作目录
# worker_class = 'gevent' #使用gevent模式，还可以使用sync 模式，默认的是sync模式

# ============================================================
# worker进程相关
# ============================================================

# 用于处理工作的进程数
workers = multiprocessing.cpu_count() * 2 + 1

# worker进程的工作方式，有sync、eventlet、gevent、tornado、gthread, 默认是sync,
# django使用gevent容易造成阻塞, 使用gthread的方式好一些
worker_class = 'gthread'

# 指定每个工作进程开启的线程数
threads = multiprocessing.cpu_count() * 2

# 访问超时时间
timeout = 30

# 接收到restart信号后，worker可以在graceful_timeout时间内，继续处理完当前requests
#graceful_timeout = 60

# server端保持连接时间
# keepalive = 30

# workers = 4 # multiprocessing.cpu_count()    #进程数
# threads = 16 #multiprocessing.cpu_count()*4 #指定每个进程开启的线程数
loglevel = 'info' #日志级别，这个日志级别指的是错误日志的级别，而访问日志的级别无法设置
access_log_format = '%(t)s %(p)s %(h)s "%(r)s" %(s)s %(L)s %(b)s %(f)s" "%(a)s"'

# pidfile='"compose/gunicorn/gunicorn.pid'

# accesslog = "/home/log/gunicorn_access.log"      #访问日志文件
#errorlog = "/home/log/gunicorn_error.log"        #错误日志文件
accesslog = str(BASE_DIR / 'laws_online/logs/access.log') #访问日志文件，"-" 表示标准输出
errorlog = str(BASE_DIR / 'laws_online/logs/error.log') #错误日志文件，"-" 表示标准输出
capture_output = True

proc_name = 'gunicorn_laws_online'   #进程名

daemon = False #后台执行