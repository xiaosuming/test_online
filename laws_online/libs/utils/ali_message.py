# -*- coding: utf-8 -*-
# This file is auto-generated, don't edit it. Thanks.
import sys
import json

from typing import List

from alibabacloud_dysmsapi20170525.client import Client as Dysmsapi20170525Client
from alibabacloud_tea_openapi import models as open_api_models
from alibabacloud_dysmsapi20170525 import models as dysmsapi_20170525_models

from laws_online.settings.base import ALI_ACCESS_KEY_ID, ALI_ACCESS_KEY_SECRET


TEMPLATE_CODE_DICT = {
    1: 'SMS_216950304',
    2: 'SMS_216950301',
    3: 'SMS_216950300',
    4: 'SMS_216950300',
    5: 'SMS_216950305'
}
#1:验证码或注册登录，2:未登录重置密码, 3:更换手机号旧手机号获取验证码, 4:更换手机号新手机号获取验证码, 5:更换身份

ERROR_MESSAGE = {
    'isv.BLACK_KEY_CONTROL_LIMIT': '发送失败',
    'isv.MOBILE_NUMBER_ILLEGAL': '请使用正确的手机号',
    'VALVE:M_MC': '次数过于频繁， 请稍后再试',
    'VALVE:H_MC': '次数过于频繁， 请稍后再试',
    'VALVE:D_MC': '今日发送次数已达上限',
    'isv.BUSINESS_LIMIT_CONTROL': '次数过于频繁， 请稍后再试',
    'OK': None,
    'oK': None,
    'ok': None,
    'Ok': None
}


class Sample:
    def __init__(self):
        pass
        # self._access_key_id = ALI_ACCESS_KEY_ID
        # self._access_key_secret = ALI_ACCESS_KEY_SECRET

    @staticmethod
    def create_client(
        # access_key_id: str,
        # access_key_secret: str,
    ) -> Dysmsapi20170525Client:
        """
        使用AK&SK初始化账号Client
        @param access_key_id:
        @param access_key_secret:
        @return: Client
        @throws Exception
        """
        config = open_api_models.Config(
            # 您的AccessKey ID,
            access_key_id=ALI_ACCESS_KEY_ID,
            # 您的AccessKey Secret,
            access_key_secret=ALI_ACCESS_KEY_SECRET
        )
        # 访问的域名
        config.endpoint = 'dysmsapi.aliyuncs.com'
        return Dysmsapi20170525Client(config)

    @staticmethod
    def main(
        args: List[str],
    ) -> None:
        client = Sample.create_client()
        send_sms_request = dysmsapi_20170525_models.SendSmsRequest(
            phone_numbers='15502127740',
            sign_name='众法在线',
            template_code='SMS_216950304',
            template_param=json.dumps({'code': '123456'})
        )
        # 复制代码运行请自行打印 API 的返回值
        print(client.send_sms(send_sms_request))

    @staticmethod
    async def main_async(
        args: List[str],
    ) -> None:
        client = Sample.create_client()
        send_sms_request = dysmsapi_20170525_models.SendSmsRequest(
            phone_numbers='15502127740',
            sign_name='众法在线',
            template_code='SMS_216950304',
            template_param=json.dumps({'code': '123456'})
        )
        # 复制代码运行请自行打印 API 的返回值
        await client.send_sms_async(send_sms_request)

    @staticmethod
    def send(
            phone_numbers: str,
            template: int,
            code: str,
            # args: List[str],
    ) -> tuple:
        client = Sample.create_client()
        send_sms_request = dysmsapi_20170525_models.SendSmsRequest(
            phone_numbers=phone_numbers,
            sign_name='众法在线',
            template_code=TEMPLATE_CODE_DICT[template],
            template_param=json.dumps({'code': code})
        )
        # 复制代码运行请自行打印 API 的返回值
        result = client.send_sms(send_sms_request)
        print(result)
        result_body = result.body.to_map()
        print(result_body)
        code = result_body['Code']
        message = ERROR_MESSAGE.get(code, '信息发送错误')
        return result_body, message


if __name__ == '__main__':
    Sample.main(sys.argv[1:])
