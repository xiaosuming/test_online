# from rest_framework import status as s
from rest_framework.response import Response as R
from rest_framework.serializers import Serializer
from django_redis import get_redis_connection
import snowflake.client
from rest_framework_xml.parsers import XMLParser
from django.views.static import serve
from laws_online.settings.base import MEDIA_ROOT
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from laws_online.libs.middlewares.CustomJSONWebTokenAuthentication import (CustomJSONWebTokenAuthentication,
                                                                           SessionAuthentication,
                                                                           BasicAuthentication)
from django.contrib.auth.models import AnonymousUser
from laws_online.apps.advertisement.models import AdvertisementPicture
from rest_framework.permissions import IsAuthenticated
# from urllib.parse import quote,unquote

# 重定义Response

class Response(R):

    def __init__(self, data=None, status=None,
                 template_name=None, headers=None,
                 exception=False, content_type=None):
        """
        Alters the init arguments slightly.
        For example, drop 'template_name', and instead use 'data'.

        Setting 'renderer' and 'media_type' will typically be deferred,
        For example being set automatically by the `APIView`.
        """
        super().__init__(None, status=status)

        if isinstance(data, Serializer):
            msg = (
                'You passed a Serializer instance as data, but '
                'probably meant to pass serialized `.data` or '
                '`.error`. representation.'
            )
            raise AssertionError(msg)

        status = status if status else 200

        if 200 <= status < 300:
            self.data = {
                'code': 0,
                'msg': 'SUCCESS',
                'data': data
            }
        else:
            self.data = {
                'code': status,
                'msg': data,
                'data': ''
            }
        self.template_name = template_name
        self.exception = exception
        self.content_type = content_type


#X-Forwarded-For:简称XFF头，它代表客户端，也就是HTTP的请求端真实的IP，只有在通过了HTTP 代理或者负载均衡服务器时才会添加该项。
def get_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]#所以这里是真实的ip
    else:
        ip = request.META.get('REMOTE_ADDR')#这里获得代理ip
    return ip


def get_snowflake():
    return snowflake.client.get_guid()


def dict_flatten(items):
    for k, v in items.items():
        if isinstance(v, dict):
            yield from dict_flatten(v)
        else:
            yield k, v


import requests, json
from laws_online.apps.city.models import City


def city_model(districts, parent=None):
    for i in districts:
        center = i['center'].split(',')
        create_data = {
            "city_code": i['citycode'] if i['citycode'] else None,
            "ad_code": i['adcode'],
            "name": i['name'],
            "longitude": center[0],
            "latitude": center[1],
            "level": i['level'],
            "parent": parent
        }
        instance = City.objects.create(**create_data)
        if i['districts']:
            city_model(i['districts'], instance)


# XMLParser 默认media_type='application/xml'
class TextXMLParser(XMLParser):
    media_type = 'text/xml'


@api_view(['GET'])
@authentication_classes([CustomJSONWebTokenAuthentication, SessionAuthentication, BasicAuthentication])
def custom_serve(request, path, document_root=None, show_indexes=False):
    user = request.user
    print(request.get_full_path())
    full_path = request.get_full_path()
    full_path_list = full_path.split('/')
    if full_path_list[2] not in ['advertisement', 'conversation']:
        if isinstance(user, AnonymousUser):
            return Response('', status=401)

    file_name = AdvertisementPicture.objects.filter(is_deleted=False, picture=full_path_list[-1]).first()
    path = file_name.picture.path

    return serve(request, path, document_root=MEDIA_ROOT, show_indexes=False)



if __name__ == '__main__':
    pass
    url = 'https://restapi.amap.com/v3/config/district?key=790a3de6c39bad23b0827ad7649112ea&keywords=中国&subdistrict=2&extensions=base'
    r = requests.get(url)
    text = r.text
    data = json.loads(text)
    # city_model(data['districts'])
