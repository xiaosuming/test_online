from rest_framework.serializers import ImageField, FileField
from rest_framework.settings import api_settings
from django.contrib.auth.models import AnonymousUser

class CustomFileField(FileField):

    def to_representation(self, value):
        if not value:
            return None

        use_url = getattr(self, 'use_url', api_settings.UPLOADED_FILES_USE_URL)
        request = self.context.get('request', None)
        if use_url:
            try:
                url = value.url
                print(16, dir(self.context.get('request')))
                url_list = url.split('/')
                url = '/'.join(url_list[:-1] +
                               [url_list[-1] if request is None or isinstance(request.user, AnonymousUser) else str(request.user.pk)])
                print(url)
            except AttributeError:
                return None

            if request is not None:
                return request.build_absolute_uri(url)
            return url

        return value.name


class CustomImageField(CustomFileField, ImageField):
    pass