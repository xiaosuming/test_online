from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework.authentication import SessionAuthentication, BasicAuthentication, authenticate, get_user_model
from rest_framework import exceptions
from rest_framework_jwt.settings import api_settings
from django.utils.translation import ugettext as _
jwt_decode_handler = api_settings.JWT_DECODE_HANDLER
import jwt


class CustomJSONWebTokenAuthentication(JSONWebTokenAuthentication):
    """
    Clients should authenticate by passing the token key in the "Authorization"
    HTTP header, prepended with the string specified in the setting
    `JWT_AUTH_HEADER_PREFIX`. For example:

        Authorization: JWT eyJhbGciOiAiSFMyNTYiLCAidHlwIj
    """

    def authenticate(self, request):
        """
        Returns a two-tuple of `User` and token if a valid signature has been
        supplied using JWT-based authentication.  Otherwise returns `None`.
        """
        jwt_value = self.get_jwt_value(request)
        if jwt_value is None:
            return None

        try:
            payload = jwt_decode_handler(jwt_value)
        except jwt.ExpiredSignature:
            msg = _('Signature has expired.')
            raise exceptions.AuthenticationFailed(msg)
        except jwt.DecodeError:
            msg = _('Error decoding signature.')
            raise exceptions.AuthenticationFailed(msg)
        except jwt.InvalidTokenError:
            raise exceptions.AuthenticationFailed()

        user = self.authenticate_credentials(payload)

        if user.is_deleted:
            msg = _('用户不存在')
            raise exceptions.AuthenticationFailed(msg)

        # if user.status != 0:
        #     msg = _('用户已被禁用， 请联系管理员')
        #     raise exceptions.AuthenticationFailed(msg)

        return (user, jwt_value)



class CustomSessionAuthentication(SessionAuthentication):
    """
    Use Django's session framework for authentication.
    """

    def authenticate(self, request):
        """
        Returns a `User` if the request session currently has a logged in user.
        Otherwise returns `None`.
        """

        # Get the session-based user from the underlying HttpRequest object
        user = getattr(request._request, 'user', None)

        # Unauthenticated, CSRF validation not required
        if not user or not user.is_active or user.is_deleted:
            return None

        self.enforce_csrf(request)

        # CSRF passed with authenticated user
        return (user, None)


class CustomBasicAuthentication(BasicAuthentication):
    """
    HTTP Basic authentication against username/password.
    """

    def authenticate_credentials(self, userid, password, request=None):
        """
        Authenticate the userid and password against username and password
        with optional request for context.
        """
        credentials = {
            get_user_model().USERNAME_FIELD: userid,
            'password': password
        }
        user = authenticate(request=request, **credentials)

        if user is None:
            raise exceptions.AuthenticationFailed(_('Invalid username/password.'))

        if not user.is_active or user.is_deleted:
            raise exceptions.AuthenticationFailed(_('User inactive or deleted.'))

        return (user, None)