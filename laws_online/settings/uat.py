from .base import *

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'laws_online',
        'USER': 'root',
        'PASSWORD': 'Flare1111',
        'HOST': '172.17.0.1',
        'PORT': '3306',
        'OPTIONS': {
            "init_command": "SET foreign_key_checks = 0;",
            'charset': 'utf8mb4'
        },
    },
}

CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://172.17.0.1:6379/10",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
            "PASSWORD": 'flare1111'
        },
        'TIMEOUT': 7200
    },
}

REDISPSWORD = 'flare1111'
REDISHOST = '172.17.0.1'
REDISPORT = 6379

# CELERY 配置
BROKER_BACKEND = 'redis'
BROKER_URL = f'redis://:{REDISPSWORD}@{REDISHOST}:{REDISPORT}/7'  # Broker配置，使用Redis作为消息中间件

CELERY_ENABLE_UTC = True

CELERY_TIMEZONE = TIME_ZONE

CELERY_RESULT_BACKEND = f'redis://:{REDISPSWORD}@{REDISHOST}:{REDISPORT}/8'  # BACKEND配置，这里使用redis

CELERY_TASK_SERIALIZER = 'pickle'

CELERY_RESULT_SERIALIZER = 'pickle'  # 结果序列化方案

CELERY_ACCEPT_CONTENT = ['pickle', 'json']

CELERYD_MAX_TASKS_PER_CHILD = 10  # 每个worker执行多少次任务后死亡，防止内存泄漏用的
