"""laws_online URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path, include
from rest_framework.documentation import include_docs_urls
from django.conf.urls.static import static
from django.views.static import serve
from rest_framework.schemas import get_schema_view
# from drf_yasg.views import get_schema_view
# from drf_yasg import openapi
from laws_online.settings.base import MEDIA_URL, MEDIA_ROOT, STATIC_ROOT
from laws_online.libs.utils.tools import custom_serve

# schema_view = get_schema_view(
#     openapi.Info(
#         title="众法在线 API",
#         default_version='v1.0',
#         description="众法在线接口文档",
#         # terms_of_service="https://www.baidu.com",
#         # contact=openapi.Contact(email="baidu@163.com"),
#         # license=openapi.License(name="BSD License"),
#     ),
#     public=True,
#     # permission_classes=(permissions.AllowAny,),
# )

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/docs/', include_docs_urls(title="文档")),
    path('api/v1/', include('laws_online.apps.users.urls')),
    path('api/v1/', include('laws_online.apps.conversation.urls')),
    path('api/v1/', include('laws_online.apps.advertisement.urls')),
    path('api/v1/', include('laws_online.apps.text.urls')),
    path('api/v1/', include('laws_online.apps.payment.urls')),
    path('api/v1/', include('laws_online.apps.suggestion_complaint.urls')),
    path('api/v1/', include('laws_online.apps.city.urls')),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    re_path(r'^static/(?P<path>.*)$', serve, {'document_root': STATIC_ROOT}, name='static'),
    # re_path(r'^upload/(?P<path>.*)', serve, {"document_root": MEDIA_ROOT}),
    # re_path(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    # path('swagger/docs/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    # path('redoc/', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
]

urlpatterns += static(MEDIA_URL, view=serve, document_root=MEDIA_ROOT)
# urlpatterns += re_path(r'^api/v1/upload/(?P<path>.*)$', serve, {'document_root': MEDIA_ROOT}, name='media'),

