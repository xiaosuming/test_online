from django.db import models
from shortuuidfield import ShortUUIDField
from django.utils.translation import gettext as _


# Create your models here.

class Payment(models.Model):
    conversation_type = models.ForeignKey('conversation.ConversationType', db_constraint=False, on_delete=models.DO_NOTHING,
                                          verbose_name='咨询类型',related_name='conversation_type_pay')
    conversation = models.ForeignKey('conversation.Conversation', db_constraint=False,
                                          on_delete=models.DO_NOTHING,
                                          verbose_name='咨询id', related_name='conversation_pay')
    from_user = models.ForeignKey('users.Users', on_delete=models.DO_NOTHING, db_constraint=False,
                                      related_name='from_user_pay', verbose_name='律师')
    to_user = models.ForeignKey('users.Users', on_delete=models.DO_NOTHING, db_constraint=False,
                               related_name='to_user_pay', verbose_name='普通用户', default=0)
    # content = models.CharField(max_length=200, default='')
    amount = models.DecimalField(max_digits=9, decimal_places=2, default=0, verbose_name='平台收入')
    origin_amount = models.DecimalField(max_digits=9, decimal_places=2, verbose_name='账单金额')
    status = models.IntegerField(default=0, verbose_name='0:未支付,1:已支付, 2:退款, 3:取消')
    settlement_status = models.IntegerField(default=0, verbose_name='结算状态')
    platform_commission = models.DecimalField(max_digits=9, decimal_places=2, verbose_name='平台利润', default=0)
    lawyer_commission = models.DecimalField(max_digits=9, decimal_places=2, verbose_name='律师收入', default=0)
    out_trade_no = models.CharField(max_length=30)
    content = models.CharField(max_length=200, default='')
    deal_time = models.DateTimeField(null=True)
    deal_date = models.DateField(null=True)
    pay_type = models.IntegerField(default=0, verbose_name='支付方式，1为支付宝，2为微信, 0为未选择')
    create_date = models.DateField(auto_now_add=True, null=True)
    create_time = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')
    update_time = models.DateTimeField(auto_now=True, verbose_name='更新时间')
    is_deleted = models.BooleanField(default=False)
    # content = models.TextField(null=True)


class AliPayRecord(models.Model):
    payment = models.ForeignKey(Payment, on_delete=models.DO_NOTHING, db_constraint=False,
                               related_name='payment_record', verbose_name='关联账单id')
    subject = models.CharField(max_length=200)
    gmt_payment = models.DateTimeField()
    charset = models.CharField(max_length=100, default='')
    seller_id = models.CharField(max_length=200)
    trade_status = models.CharField(max_length=200)
    buyer_id = models.CharField(max_length=200)
    auth_app_id = models.CharField(max_length=150, default='')
    buyer_pay_amount = models.DecimalField(max_digits=9, decimal_places=2, default=0)
    version = models.CharField(max_length=150, default='')
    gmt_create = models.DateTimeField()
    trade_no = models.CharField(max_length=150)
    fund_bill_list = models.CharField(max_length=150)
    app_id = models.CharField(max_length=150)
    notify_time = models.DateTimeField(max_length=150, null=True)
    point_amount = models.DecimalField(max_digits=9, decimal_places=2, null=True)
    total_amount = models.DecimalField(max_digits=9, decimal_places=2)
    notify_type = models.CharField(max_length=150, default='')
    out_trade_no = models.CharField(max_length=150)
    buyer_logon_id = models.CharField(max_length=150, default='')
    notify_id = models.CharField(max_length=150, default='')
    seller_email = models.CharField(max_length=150, default='')
    refund_fee = models.DecimalField(max_digits=9, decimal_places=2, null=True)
    receipt_amount = models.DecimalField(max_digits=9, decimal_places=2, null=True)
    invoice_amount = models.DecimalField(max_digits=9, decimal_places=2, null=True)
    sign = models.CharField(max_length=500, default='')
    create_time = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')
    update_time = models.DateTimeField(auto_now=True, verbose_name='更新时间')
    create_date = models.DateField(auto_now_add=True, null=True)

    is_deleted = models.BooleanField(default=False)


class WechatPayRecord(models.Model):
    payment = models.ForeignKey(Payment, on_delete=models.DO_NOTHING, db_constraint=False,
                                related_name='payment_wechat_record', verbose_name='关联账单id')
    appid = models.CharField(max_length=120)
    mch_id = models.CharField(max_length=120)
    device_info = models.CharField(max_length=120, default='')
    nonce_str = models.CharField(max_length=120)
    sign = models.CharField(max_length=120)
    sign_type = models.CharField(max_length=120, default='')
    result_code = models.CharField(max_length=120)
    err_code = models.CharField(max_length=120, default='')
    err_code_des = models.CharField(max_length=150, default='')
    openid = models.CharField(max_length=150)
    is_subscribe = models.CharField(max_length=120)
    trade_type = models.CharField(max_length=120)
    bank_type = models.CharField(max_length=120)
    total_fee = models.DecimalField(max_digits=9, decimal_places=2)
    settlement_total_fee = models.DecimalField(max_digits=9, decimal_places=2, null=True)
    fee_type = models.CharField(max_length=120, default='')
    cash_fee = models.DecimalField(max_digits=9, decimal_places=2)
    cash_fee_type = models.CharField(max_length=120, default='')
    coupon_fee = models.DecimalField(max_digits=9, decimal_places=2, null=True)
    coupon_count = models.IntegerField(default=0)
    transaction_id = models.CharField(max_length=120)
    out_trade_no = models.CharField(max_length=120)
    attach = models.CharField(max_length=120, default='')
    time_end = models.CharField(max_length=120)
    return_code = models.CharField(max_length=200, default='')
    return_msg = models.CharField(max_length=200, default='')

    create_time = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')
    update_time = models.DateTimeField(auto_now=True, verbose_name='更新时间')
    create_date = models.DateField(auto_now_add=True, null=True)

    is_deleted = models.BooleanField(default=False)


class RefundRecord(models.Model):
    payment = models.ForeignKey(Payment, db_constraint=False,
                                          on_delete=models.DO_NOTHING,
                                          verbose_name='支付id', related_name='refund_payment')
    conversation = models.ForeignKey('conversation.Conversation', db_constraint=False,
                                          on_delete=models.DO_NOTHING,
                                          verbose_name='咨询id', related_name='refund_conversation')
    from_user = models.ForeignKey('users.Users', on_delete=models.DO_NOTHING, db_constraint=False,
                                      related_name='from_user_refund', verbose_name='律师')
    to_user = models.ForeignKey('users.Users', on_delete=models.DO_NOTHING, db_constraint=False,
                               related_name='to_user_refund', verbose_name='普通用户')
    status = models.IntegerField(default=0, verbose_name='0:未处理,1:已退款, 2:退款, 3:驳回')
    amount = models.DecimalField(max_digits=9, decimal_places=2, default=0, verbose_name='退款金额')
    payed_amount = models.DecimalField(max_digits=9, decimal_places=2, default=0, verbose_name='已支付金额')
    content = models.CharField(max_length=255, verbose_name='退款原因')
    contact = models.CharField(max_length=150, verbose_name='联系方式')
    unique_id = models.CharField(max_length=30)
    create_time = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')
    update_time = models.DateTimeField(auto_now=True, verbose_name='更新时间')
    create_date = models.DateField(auto_now_add=True, null=True)
    is_deleted = models.BooleanField(default=False)



# class Converstaion(models.Model):
#     ordinary_user = models.ForeignKey('apps.users.User', on_delete=models.DO_NOTHING, db_constraint=False,
#                                       related_name='ordinary_order', verbose_name='普通用户')
#     lawyer = models.ForeignKey('users.User', on_delete=models.DO_NOTHING, db_constraint=False,
#                                related_name='lawyer_order', verbose_name='律师')
#     conversation = models.ForeignKey('conversation.Conversation', on_delete=models.DO_NOTHING, db_constraint=False,
#                                       related_name='ordinary_order', verbose_name='普通用户')
#     create_time = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')
#     update_time = models.DateTimeField(auto_now=True, verbose_name='更新时间')
#     create_by = models.IntegerField(verbose_name='创建者id')
#     is_deleted = models.BooleanField(default=False)
