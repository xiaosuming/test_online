# Generated by Django 3.1.5 on 2021-01-20 19:32

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('payment', '0008_payment_content'),
    ]

    operations = [
        migrations.RenameField(
            model_name='payment',
            old_name='trade_no',
            new_name='out_trade_no',
        ),
        migrations.RemoveField(
            model_name='payment',
            name='content',
        ),
    ]
