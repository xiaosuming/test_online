# Generated by Django 3.1.5 on 2021-01-21 17:47

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('payment', '0011_auto_20210120_2014'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='payment',
            name='lawyer',
        ),
        migrations.RemoveField(
            model_name='payment',
            name='ordinary_user',
        ),
        migrations.AddField(
            model_name='payment',
            name='from_user',
            field=models.ForeignKey(db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='from_user_pay', to=settings.AUTH_USER_MODEL, verbose_name='普通用户'),
        ),
        migrations.AddField(
            model_name='payment',
            name='to_user',
            field=models.ForeignKey(db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='to_user_pay', to=settings.AUTH_USER_MODEL, verbose_name='律师'),
        ),
    ]
