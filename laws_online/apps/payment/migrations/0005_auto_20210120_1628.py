# Generated by Django 3.1.5 on 2021-01-20 16:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('payment', '0004_payment_trade_no'),
    ]

    operations = [
        migrations.AlterField(
            model_name='payment',
            name='trade_no',
            field=models.BigIntegerField(),
        ),
    ]
