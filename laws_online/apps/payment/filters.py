from django_filters import rest_framework as filters
from .models import Payment

class PaymentSearch(filters.FilterSet):

    class Meta:
        model = Payment
        fields = 'conversation',


class LawyerPaymentSearch(filters.FilterSet):
    create_time = filters.DateTimeFromToRangeFilter(field_name='create_time')
    id = filters.NumberFilter(field_name='pk', lookup_expr='lt')

    class Meta:
        fields = 'create_time', 'id',