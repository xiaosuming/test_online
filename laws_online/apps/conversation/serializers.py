from rest_framework import serializers
from .models import Conversation, ConversationType
from rest_framework.serializers import ValidationError
from laws_online.apps.users.models import Users
from django.contrib.auth.models import AnonymousUser
from laws_online.libs.utils.tools import dict_flatten, get_snowflake
from django.utils import timezone
# from laws_online.apps.suggestion_complaint.models import LawyerEvaluation, LawyerComplaint


class C2CCallbackAfterSendMsgSerializer(serializers.Serializer):
    CallbackCommand = serializers.CharField()
    # From_Account = serializers.CharField()
    # To_Account = serializers.CharField()
    # MsgSeq = serializers.IntegerField()
    # MsgRandom = serializers.IntegerField()
    # MsgTime = serializers.IntegerField()
    # MsgKey = serializers.CharField()
    # SendMsgResult = serializers.CharField()
    # ErrorInfo = serializers.CharField()

class LawyerLawFirmSerialzier(serializers.ModelSerializer):
    law_firm = serializers.CharField()
    city = serializers.SerializerMethodField()

    def get_city(self, obj):
        return f'{obj.province_name} {obj.city_name}'

    class Meta:
        model = Users
        fields = 'law_firm', 'city'


class LawyerSimpleInfoSerializer(serializers.ModelSerializer):
    lawyer = LawyerLawFirmSerialzier(source='lawyer_user')
    picture = serializers.ImageField()

    class Meta:
        model = Users
        fields = 'id', 'uid', 'nickname', 'picture', 'lawyer', 'phone'


class ConversationSerializer(serializers.ModelSerializer):
    status = serializers.ChoiceField(choices=[0], default=0, help_text='(int)0表示未开启，1表示开启， 2表示取消， 3表示已结束')
    type = serializers.PrimaryKeyRelatedField(queryset=ConversationType.objects.filter(is_deleted=False), help_text='(int)领域类型id')
    content = serializers.CharField(max_length=200, help_text='(str)咨询详情，最大不超过200')
    # ordinary_user = serializers.PrimaryKeyRelatedField(queryset=Users.objects.filter(is_deleted=False, is_active=True), help_text='')
    lawyer_id = serializers.PrimaryKeyRelatedField(queryset=Users.objects.filter(is_deleted=False, is_active=True),
                                                   required=False, help_text='律师id', write_only=True, allow_null=True,
                                                   default=None)
    type_name = serializers.CharField(read_only=True, source='type.name')
    user_picture = serializers.ImageField(source='ordinary_user.picture', read_only=True)
    user_uid = serializers.CharField(source='ordinary_user.uid', read_only=True)
    user_phone = serializers.SerializerMethodField(source='ordinary_user.phone', read_only=True)
    lawyer = serializers.SerializerMethodField(read_only=True)
    start_now = serializers.BooleanField(write_only=True, default=False, help_text='是否需要创建时就开始')
    unique_id = serializers.CharField(read_only=True)
    complaint = serializers.SerializerMethodField()
    suggestion = serializers.SerializerMethodField()

    def get_lawyer(self, obj):
        lawyer = obj.lawyer
        return dict(dict_flatten(LawyerSimpleInfoSerializer(lawyer, context=self.context).data)) if lawyer else None

    def get_complaint(self, obj):
        user = self.context.get('request').user
        if not isinstance(user, AnonymousUser):
            if user in [obj.lawyer, obj.ordinary_user]:
                return [i.pk for i in obj.complaint_conversation.filter(is_deleted=False, from_user=user)]
            return []
        return []

    def get_suggestion(self, obj):
        user = self.context.get('request').user
        if isinstance(user, AnonymousUser):
            return []
        if user == obj.ordinary_user:
            return [i.pk for i in obj.evaluation_conversation.filter(is_deleted=False)]
        if user == obj.lawyer:
            return [i.pk for i in obj.evaluation_conversation_by_lawyer.filter(is_deleted=False)]
        return []

    def get_user_phone(self, obj):
        phone = obj.ordinary_user.phone
        return f'用户{phone[0:3]}****{phone[-4:]}'

    def validate(self, attrs):
        if attrs.get('start_now') and not attrs.get('lawyer_id'):
            raise ValidationError('缺少参数')
        return attrs

    class Meta:
        model = Conversation
        fields = ('id', 'type', 'status', 'create_time', 'content', 'lawyer', 'type_name', 'ordinary_user',
                  'user_picture', 'user_uid', 'user_phone', 'unique_id', 'update_time', 'start_now', 'lawyer_id',
                  'complaint', 'suggestion')
        read_only_fields = ('create_time', 'lawyer', 'ordinary_user', 'user_picture', 'user_uid', 'user_phone',
                            'unique_id')


class ConversationBasicSerializer(serializers.ModelSerializer):

    class Meta:
        model = Conversation
        fields = ('id', 'type', 'status', 'create_time', 'content', 'lawyer', 'type', 'ordinary_user', 'create_time',
                  'unique_id', 'open_time', 'notice_time')


class RestartConversationSerializer(serializers.Serializer):
    lawyer = serializers.PrimaryKeyRelatedField(queryset=Users.objects.filter(is_deleted=False, role=300),
                                                help_text='律师id')

    def create(self, validated_data):
        conversation = validated_data['instance']
        new_conversation = Conversation()
        new_conversation.ordinary_user = self.context.get('request').user
        new_conversation.lawyer = validated_data['lawyer']
        new_conversation.unique_id = get_snowflake()
        new_conversation.status = 1
        new_conversation.type = conversation.type
        new_conversation.content = conversation.content
        new_conversation.open_time = timezone.now()
        new_conversation.save()
        return new_conversation


# class ConversationRetrieveSerializer(ConversationSerializer):
#     complaint = serializers.SerializerMethodField()
#     suggestion = serializers.SerializerMethodField()
#
#     def get_complaint(self, obj):
#         return [i.pk for i in obj.complaint_conversation.filter(is_deleted=False)]
#
#     def get_suggestion(self, obj):
#         return [i.pk for i in obj.evaluation_conversation.filter(is_deleted=False)]
#
#     class Meta:
#         model = Conversation
#         fields = ('id', 'type', 'status', 'create_time', 'content', 'lawyer', 'type_name', 'ordinary_user',
#                   'user_picture', 'user_uid', 'user_phone', 'unique_id', 'update_time', 'start_now', 'lawyer_id',
#                   'complaint', 'suggestion')




class ConversationTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ConversationType
        fields = ('id', 'name', 'picture')


class LawyerNameAndPictureSerializer(serializers.ModelSerializer):
    nickname = serializers.CharField()
    picture = serializers.ImageField()

    class Meta:
        model = Users
        fields = 'nickname', 'picture', 'id', 'uid', 'is_online'


class ConversationUserSerializer(serializers.ModelSerializer):
    lawyer = LawyerNameAndPictureSerializer()
    type_name = serializers.CharField(source='type.name')
    unique_id = serializers.CharField(read_only=True)

    # aa = serializers.CharField(source='ordinary_user.nickname')

    # def get_lawyer_name(self, obj):
    #     '''
    #     待优化
    #     '''
    #     lawyer = obj.lawyer
    #     return lawyer.nickname if lawyer is not None else None
    #
    # def get_lawyer_picture(self, obj):
    #     '''
    #     待优化
    #     '''
    #     lawyer = obj.lawyer
    #     return lawyer.picture if lawyer is not None else None
    #     return lawyer.picture.url if (lawyer.picture if lawyer else None) is not None else None

    class Meta:
        model = Conversation
        fields = ('id', 'status', 'create_time', 'lawyer', 'content', 'type_name', 'unique_id', 'update_time')


class UserSimpleInfoSerialzier(serializers.ModelSerializer):
    user_phone = serializers.CharField(source='phone')
    user_picture = serializers.ImageField(source='picture')
    ordinary_user = serializers.IntegerField(source='id')

    class Meta:
        model = Users
        fields = ('user_phone', 'user_picture', 'uid', 'ordinary_user')


class UserSimpleInfoSerialzier2(serializers.ModelSerializer):
    user_phone = serializers.SerializerMethodField()
    user_picture = serializers.ImageField(source='picture')
    ordinary_user = serializers.IntegerField(source='id')

    def get_user_phone(self, obj):
        return f'用户{obj.phone[0:3]}****{obj.phone[-4:]}'

    class Meta:
        model = Users
        fields = ('user_phone', 'user_picture', 'uid', 'ordinary_user')


class OrdinaryUserSerializer(serializers.ModelSerializer):
    user_phone = serializers.SerializerMethodField()
    user_picture = serializers.ImageField(source='picture')
    ordinary_user = serializers.IntegerField(source='id')

    def get_user_phone(self, obj):
        return f'用户{obj.phone[0:3]}****{obj.phone[-4:]}'

    class Meta:
        model = Users
        fields = ('user_phone', 'user_picture', 'uid', 'ordinary_user', 'is_online')


class LawyerConversationSerializer(serializers.ModelSerializer):
    conversation_type = serializers.CharField(source='type.name')
    ordinary_user = UserSimpleInfoSerialzier2()

    class Meta:
        model = Conversation
        fields = ('id', 'content', 'create_time', 'conversation_type', 'ordinary_user', 'status', 'type')



class LawyerConversationSerializer2(serializers.ModelSerializer):
    conversation_type = serializers.CharField(source='type.name')
    ordinary_user = OrdinaryUserSerializer()

    class Meta:
        model = Conversation
        fields = ('id',  'content', 'create_time', 'conversation_type', 'ordinary_user', 'status', 'type')


class StartConversationSerializer(serializers.ModelSerializer):
    another_user = serializers.PrimaryKeyRelatedField(write_only=True,
        queryset=Users.objects.filter(is_deleted=False), help_text='律师或者用户id')


    class Meta:
        model = Conversation
        fields =('id', 'ordinary_user', 'lawyer', 'another_user')
        read_only_fields = ('id', 'ordinary_user', 'lawyer')


class UserInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Users
        fields = 'id', 'picture', 'uid', 'phone'


class RecentConversationListSerializer(serializers.ModelSerializer):
    lawyer = serializers.SerializerMethodField()
    ordinary_user = UserInfoSerializer()
    unique_id = serializers.CharField(read_only=True)

    def get_lawyer(self, obj):
        return dict(dict_flatten(LawyerSimpleInfoSerializer(obj.lawyer).data))

    class Meta:
        model = Conversation
        fields =('id', 'type', 'status', 'create_time', 'content', 'lawyer', 'ordinary_user', 'unique_id', 'open_time')




class NoneSerializer(serializers.Serializer):
    pass