from django_filters import rest_framework as filters
from .models import Conversation
from laws_online.apps.city.models import City

class UserConversationSearch(filters.FilterSet):
    status = filters.CharFilter(field_name='status', lookup_expr='exact')

    class Meta:
        model = Conversation
        fields = 'status',


class LawyerConversationSearch(filters.FilterSet):
    city = filters.CharFilter(method='city_filter')
    type = filters.CharFilter(method='type_filter')

    def city_filter(self, queryset, name, value):
        return queryset
        city = City.objects.filter(ad_code=value)
        if city is None:
            return queryset
        return queryset.filter(lawyer__lawyer_user__city=city)

    def type_filter(self, queryset, name, value):
        if value not in ['100', '200', 100, 200]:
            return queryset
        return queryset.filter(ordinary_user__role=value)

    class Meta:
        model = Conversation
        fields = ('city', 'type')