from django.db import models
from django.utils import timezone
from shortuuidfield import ShortUUIDField
from django.utils.translation import gettext as _
# import snowflake.client

# Create your models here.

class ConversationType(models.Model):
    name = models.CharField(max_length=50)
    create_time = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')
    update_time = models.DateTimeField(auto_now=True, verbose_name='更新时间')
    picture = models.ImageField(verbose_name='图标', null=True, upload_to="conversation/type/picture")
    is_deleted = models.BooleanField(default=False)


class Conversation(models.Model):
    ordinary_user = models.ForeignKey('users.Users', on_delete=models.DO_NOTHING, db_constraint=False,
                                      related_name='ordinary_conversation', verbose_name='普通用户')
    lawyer = models.ForeignKey('users.Users', on_delete=models.DO_NOTHING, db_constraint=False,
                               related_name='lawyer_conversation', verbose_name='律师', null=True)
    type = models.ForeignKey(ConversationType, on_delete=models.DO_NOTHING, db_constraint=False,
                               related_name='conversation_type', verbose_name='会话类型（领域）')
    unique_id = models.CharField(max_length=30, default=0)
    status = models.IntegerField(verbose_name='会话状态')  # 0表示未开启，1表示开启， 2表示取消， 3表示已结束
    content = models.CharField(max_length=200, verbose_name='问题描述', default='')
    close_time = models.DateTimeField(null=True)
    create_date = models.DateField(auto_now_add=True, null=True)
    create_time = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')
    update_time = models.DateTimeField(auto_now=True, verbose_name='更新时间')

    open_time = models.DateTimeField(null=True, verbose_name='订单开启时间')

    # notice = models.DateTimeField(default=timezone.now, verbose_name='是否需要提醒')
    notice_time = models.DateTimeField(default=timezone.now, verbose_name='提醒时间， 用作标记')

    is_deleted = models.BooleanField(default=False)


class ChatRecord(models.Model):
    from_user = models.ForeignKey('users.Users', on_delete=models.DO_NOTHING, db_constraint=False,
                                      related_name='from_user_chat_record', verbose_name='消息发送者')
    to_user = models.ForeignKey('users.Users', on_delete=models.DO_NOTHING, db_constraint=False,
                               related_name='to_from_user_chat_record', verbose_name='消息接受者')
    msg_status = models.IntegerField()
    msg_time = models.DateTimeField()
    msg_random = models.CharField(max_length=300)
    msg_seq = models.IntegerField()
    msg_key = models.CharField(max_length=100)
    error_info = models.CharField(max_length=120)
    type = models.CharField(max_length=100,verbose_name='消息类型')
    content = models.TextField(verbose_name='消息内容')
    create_time = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')
    update_time = models.DateTimeField(auto_now=True, verbose_name='更新时间')
    is_deleted = models.BooleanField(default=False)
