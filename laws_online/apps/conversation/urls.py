from django.urls import path, include
from . import views
from rest_framework import routers
# from rest_framework_jwt.views import obtain_jwt_token
# from rest_framework_sso.authentication import JWTAuthentication

router = routers.DefaultRouter(trailing_slash=False)
router.register(r'im_callback', views.IMCallback, 'im_callback')
router.register(r'info', views.ConversationView, 'conversations')
router.register(r'type', views.ConversationTypeView, 'conversation_type')
router.register(r'for_lawyer', views.LawyerConversationView, 'lawyer_conversation')
# router.register(r'user', views.UserConversationView, 'conversation_user')
# router.register(r'family', views.GetFamilyInfoView, 'family_info')
# router.register(r'platform', views.GetPlatformDataView, 'platform')
# router.register(r'family_information', views.FamilyInformationView, 'family_public_information')


urlpatterns = [
    path('conversation/', include((router.urls, 'laws_online'), namespace='conversation')),
]
