from django.shortcuts import render
# from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework.viewsets import ModelViewSet, GenericViewSet, ReadOnlyModelViewSet
from rest_framework.mixins import CreateModelMixin, RetrieveModelMixin, ListModelMixin
from rest_framework import status
from .models import Conversation, ConversationType, ChatRecord
from laws_online.apps.users.models import Users, EnterpriseUser
from .serializers import (C2CCallbackAfterSendMsgSerializer, ConversationSerializer, ConversationTypeSerializer,
                          ConversationUserSerializer, LawyerConversationSerializer, StartConversationSerializer,
                          NoneSerializer, RecentConversationListSerializer, LawyerConversationSerializer2,
                          ConversationBasicSerializer, RestartConversationSerializer)
from rest_framework.permissions import AllowAny, IsAuthenticated
from laws_online.libs.utils.tools import Response
from laws_online.apps.payment.models import Payment
from laws_online.apps.suggestion_complaint.models import OrdinaryUserEvaluation, LawyerEvaluation
from rest_framework.response import Response as OriginResponse
from laws_online.settings.base import MEDIA_URL
import datetime
from rest_framework.decorators import action
from laws_online.libs.middlewares.CustomPermission import (LawyerUserPermission, OrdinaryUserPermission,
    IsAuthenticatedByManager, IsAuthenticatedAndNoBanned)
from django_filters.rest_framework import DjangoFilterBackend
from .filters import UserConversationSearch, LawyerConversationSearch
from django.core.cache import cache
from django_redis import get_redis_connection
from django.db.models import Q
from django.db import transaction
from django.utils import timezone
from laws_online.apps.users.utils import post_im_message
from laws_online.libs.utils.tools import get_snowflake, dict_flatten
import json
import datetime


# from rest_framework.filters import OrderingFilter


# Create your views here.

class IMCallback(GenericViewSet, CreateModelMixin):
    serializer_class = C2CCallbackAfterSendMsgSerializer
    queryset = Conversation.objects.filter(is_deleted=0)
    permission_classes = [AllowAny]

    def create(self, request, *args, **kwargs):
        '''
        :param request:
        :param args:
        :param kwargs:
        :return: 该接口为接受聊天回调接口，请勿使用
        '''
        callback_command = request.query_params.get('CallbackCommand')
        response_data = {
            "ActionStatus": "OK",
            "ErrorInfo": "",
            "ErrorCode": 0
        }
        if callback_command == 'C2C.CallbackAfterSendMsg':
            data = request.data
            if data['CallbackCommand'] != 'C2C.CallbackAfterSendMsg':
                response_data['ErrorCode'] = 1
                return OriginResponse(response_data, status=status.HTTP_405_METHOD_NOT_ALLOWED)

            from_user = Users.objects.filter(uid=data['From_Account']).first()
            to_user = Users.objects.filter(uid=data['To_Account']).first()

            if not from_user or not to_user:
                response_data['ErrorCode'] = 1
                return OriginResponse(response_data, status=status.HTTP_400_BAD_REQUEST)

            msg_time = datetime.datetime.fromtimestamp(data['MsgTime'])
            content_data = (data['MsgBody'][0]['MsgContent']['Text'] if  data['MsgBody'][0]['MsgContent'].get('Text')
                            else data['MsgBody'][0]['MsgContent']['Data'])
            ChatRecord.objects.create(from_user=from_user, to_user=to_user, msg_seq=data['MsgSeq'],
                                      msg_random=data['MsgRandom'], msg_time=msg_time,
                                      msg_key=data['MsgKey'], msg_status=data['SendMsgResult'],
                                      error_info=data['ErrorInfo'], type=data['MsgBody'][0]['MsgType'],
                                      content=content_data)

            return OriginResponse(response_data)
        elif callback_command == 'C2C.CallbackBeforeSendMsg':
            response_data = {
                "ActionStatus": "OK",
                "ErrorInfo": "",
                "ErrorCode": 0
            }
            return OriginResponse(response_data, status=status.HTTP_200_OK)
            data = request.data
            if data['MsgBody'][0]['MsgType'] == 'TIMCustomElem':
                msg_data = data['MsgBody'][0]['MsgContent'].get('Data')
                msg_data = json.loads(msg_data)
                conversation_id = msg_data['conversation']
                # conversation_type_id = msg_data['conversation_type']
                msg_type = msg_data['type']
                amount = msg_data['amount']
                if msg_type == 100:  # 律师发起订单
                    from_user = Users.objects.filter(uid=data['From_Account'], is_deleted=False, status=0).first()
                    to_user = Users.objects.filter(uid=data['To_Account'], is_deleted=False, status=0).first()

                    if not from_user or not to_user:
                        response_data['ErrorCode'] = 1
                        return OriginResponse(response_data, status=status.HTTP_400_BAD_REQUEST)

                    conversation = Conversation.objects.filter(is_deleted=False, pk=conversation_id,
                                                               ordinary_user=to_user, lawyer=from_user).first()

                    if not conversation:
                        response_data['ErrorCode'] = 1
                        return OriginResponse(response_data, status=status.HTTP_400_BAD_REQUEST)

                    out_trade_no = get_snowflake()
                    conversation_type = conversation.type
                    payment = Payment.objects.create(conversation=conversation, from_user=from_user, to_user=to_user,
                                                     conversation_type=conversation_type, amount=amount,
                                                     out_trade_no=out_trade_no)
                    msg_body = [
                        {
                            'MsgType': 'TIMCustomElem',
                            'MsgContent': {
                                'Data': json.dumps({
                                    'payment_id': payment.pk,
                                    'out_trade_no': out_trade_no,
                                    'conversation_type': conversation_type.name,
                                    'content': conversation.content,
                                    **msg_data
                                }, ensure_ascii=False)
                            }
                        }
                    ]
                    response_data['MsgBody'] = msg_body

                    msg_time = datetime.datetime.fromtimestamp(data['MsgTime'])
                    ChatRecord.objects.create(from_user=from_user, to_user=to_user, msg_seq=data['MsgSeq'],
                                              msg_random=data['MsgRandom'], msg_time=msg_time,
                                              msg_key=data['MsgKey'], msg_status=data.get('SendMsgResult', -1),
                                              error_info=data.get('ErrorInfo', '-1'), type=data['MsgBody'][0]['MsgType'],
                                              content=data['MsgBody'][0]['MsgContent']['Data'])

                    return OriginResponse(response_data, status=status.HTTP_200_OK)
            else:
                # response_data['ErrorCode'] = 1
                # response_data['ActionStatus'] = 'Fail'
                return OriginResponse(response_data)

        print(request.data)
        print(22)

        return OriginResponse(1, status=status.HTTP_402_PAYMENT_REQUIRED)


class ConversationView(GenericViewSet, CreateModelMixin, RetrieveModelMixin):
    serializer_class = ConversationSerializer
    queryset = Conversation.objects.filter(is_deleted=False)
    filter_class = UserConversationSearch
    filter_backends = (DjangoFilterBackend,)

    def retrieve(self, request, *args, **kwargs):
        '''
        获取咨询详情
        '''
        instance = self.get_object()
        instance = self.queryset.filter(pk=instance.pk).select_related('lawyer', 'ordinary_user')
        # if request.user not in [instance.lawyer, instance.ordinary_user]:
        #     return Response('权限错误', status=status.HTTP_403_FORBIDDEN)
        # user = request.user
        # if user not in [instance.ordinary_user, instance.lawyer]:
        #     return Response("用户无权限", status=status.HTTP_403_FORBIDDEN)
        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    # def list(self, request, *args, **kwargs):
    #     '''
    #     获取我的咨询列表
    #     '''
    #     queryset = self.filter_queryset(self.get_queryset().filter(ordinary_user=request.user))
    #     return super().list(request, *args, **kwargs)

    @transaction.atomic
    def create(self, request, *args, **kwargs):
        '''
        创建咨询
        '''
        if request.user.status != 0:
            return Response('账户已被禁用，请联系平台工作人员', status=status.HTTP_403_FORBIDDEN)

        if request.user.role not in [100, 200, 1000]:
            return Response('律师身份不能发布咨询', status=status.HTTP_403_FORBIDDEN)

        if request.user.role == 200:
            if request.user.is_authentication in [0, 1]:
                return Response('企业未认证', status=status.HTTP_400_BAD_REQUEST)
            if request.user.is_authentication == 3:
                try:
                    request.user.enterprise_user
                except EnterpriseUser.DoesNotExist:
                    return Response('企业未认证', status=status.HTTP_400_BAD_REQUEST)

        if Conversation.objects.filter(ordinary_user=request.user,
                                       is_deleted=False, status__in=[0, 1])[:3].count() == 3:
            return Response('您当前咨询数量已超过上限', status=status.HTTP_400_BAD_REQUEST)
        if Conversation.objects.filter(ordinary_user=request.user,
                                       is_deleted=False, status__in=[0, 1])[:5].count() == 5:
            return Response('该律师接单数已超过上限', status=status.HTTP_400_BAD_REQUEST)

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.validated_data['ordinary_user'] = request.user
        serializer.validated_data['unique_id'] = get_snowflake()
        data = serializer.validated_data
        start_now = data.pop('start_now')
        data['lawyer'] = data.pop('lawyer_id')
        lawyer_uid = data['lawyer'].uid if data['lawyer'] else None
        data['status'] = 1 if start_now else 0

        if start_now and request.user.unionid == data['lawyer'].unionid:
            return Response('不能咨询自己发布的咨询', status=status.HTTP_400_BAD_REQUEST)

        if start_now and Conversation.objects.filter(ordinary_user=request.user, is_deleted=False, status=1,
                                                     lawyer=data['lawyer']).exists():
            return Response('已存在与该律师的咨询', status=status.HTTP_400_BAD_REQUEST)

        if start_now:
            data['open_time'] = timezone.now()

        save_id = transaction.savepoint()
        instance = serializer.create(data)

        if start_now:
            im_data = {
                'type': 200,
                'title': '咨询订单',
                'conversation': instance.id,
                'conversation_type': instance.type.name,
                'content': instance.content
            }
            print(lawyer_uid)
            print(request.user.uid)
            print(im_data)
            if not post_im_message(request.user.uid, lawyer_uid, im_data):
                transaction.savepoint_rollback(save_id)
                return Response('创建失败', status=status.HTTP_400_BAD_REQUEST)

        transaction.savepoint_commit(save_id)
        return Response({'id': instance.pk}, status=status.HTTP_201_CREATED)

    @action(methods=['GET'], detail=False, url_path='user',
            serializer_class=ConversationUserSerializer, filter_backends=(DjangoFilterBackend,),
            filter_class=UserConversationSearch)
    def get_user_conversation_list(self, request, *args, **kwargs):
        '''
        获取咨询列表（我的咨询模块）,可接受传参status，不传显示所有，0表示未解决，1表示进行中， 2表示取消， 3表示已结束
        路径参考 /api/v1/conversation/info/user?status=0
        '''
        user = request.user
        if user.role in [100, 200]:
            queryset = self.filter_queryset(self.get_queryset().filter(ordinary_user=user).select_related('lawyer').
                                            order_by('-create_time'))
        elif user.role == 300:
            queryset = self.filter_queryset(self.get_queryset().filter(lawyer=user).select_related('ordinary_user').
                                            order_by('-create_time'))
        else:
            return Response('权限错误', status=status.HTTP_403_FORBIDDEN)

        page = self.paginate_queryset(queryset)
        page_index = request.query_params.get('page')
        if page is not None and page_index:
            if user.role == 300:
                serializer = LawyerConversationSerializer2(page, many=True, context={'request': request})
                result_data = [dict(dict_flatten(i)) for i in serializer.data]
                conversation_ids = [i['id'] for i in result_data]
                evaluations = OrdinaryUserEvaluation.objects.filter(conversation__in=conversation_ids,
                                                                    is_deleted=False).values('id', 'conversation')
                evaluations_dict = {}
                for i in evaluations:
                    evaluation = evaluations_dict.get(i['conversation'])
                    if evaluation:
                        evaluations_dict[i['conversation']].append(i['id'])
                    else:
                        evaluations_dict[i['conversation']] = [i['id']]


                for i, v in enumerate(result_data):
                    result_data[i]['evaluation'] = evaluations_dict.get(v['id'], [])

                return self.get_paginated_response(result_data)

            serializer = self.get_serializer(page, many=True)
            result_data = serializer.data
            conversation_ids = [i['id'] for i in result_data]
            evaluations = LawyerEvaluation.objects.filter(conversation__in=conversation_ids,
                                                                is_deleted=False).values('id', 'conversation')
            evaluations_dict = {}
            for i in evaluations:
                evaluation = evaluations_dict.get(i['conversation'])
                if evaluation:
                    evaluations_dict[i['conversation']].append(i['id'])
                else:
                    evaluations_dict[i['conversation']] = [i['id']]

            for i, v in enumerate(result_data):
                result_data[i]['evaluation'] = evaluations_dict.get(v['id'], [])

            return self.get_paginated_response(result_data)

        return Response('缺少分页参数', status=status.HTTP_400_BAD_REQUEST)

        # for i, v in enumerate(serializer.data):
        #     user_ids.append(v['ordinary_user'])
        #     conversation_type_ids.append(v['type'])
        #
        # users = Users.objects.filter(pk__in=user_ids, is_deleted=False)
        # conversation_types = ConversationType.objects.filter(pk__in=conversation_type_ids)
        # user_dict = {i.pk: [i, f'用户{i.phone[0:3]}****{i.phone[-4:]}'] for i in users}
        # conversation_types_dict = {i.pk: i.name for i in conversation_types}
        #
        # for i, v in enumerate(serializer.data):
        #     serializer.data[i]['user_phone'] = user_dict[v['ordinary_user']][1]
        #     picture = user_dict[v['ordinary_user']][0].picture
        #     serializer.data[i][
        #         'user_picture'] = f"{request.META['wsgi.url_scheme']}://{request.get_host()}{MEDIA_URL}{picture}" if picture else None
        #     serializer.data[i]['uid'] = user_dict[v['ordinary_user']][0].uid
        #     # serializer.data[i].pop('ordinary_user')
        #     serializer.data[i]['conversation_type'] = conversation_types_dict[v['type']]
        #     serializer.data[i].pop('type')


        # serializer = self.get_serializer(queryset, many=True)
        # d = serializer.data
        # from django.db import connection
        # for i in connection.queries:
        #     print(i)
        # return Response({'result': serializer.data})

    @action(methods=['PUT'], detail=True, url_path='open', permission_classes=[IsAuthenticatedByManager],
            serializer_class=StartConversationSerializer)
    def open_conversation(self, request, pk, *args, **kwargs):
        '''
        开始咨询
        '''
        instance = self.get_object()

        if request.user.status != 0 :
            return Response('账户已被禁用，请联系平台工作人员', status=status.HTTP_403_FORBIDDEN)

        if request.user.role == 100:
            pass
        if request.user.role == 200:
            if request.user.is_authentication in [0, 1]:
                return Response('企业未认证', status=status.HTTP_400_BAD_REQUEST)
            if request.user.is_authentication == 3:
                try:
                    request.user.enterprise_user
                except EnterpriseUser.DoesNotExist:
                    return Response('企业未认证', status=status.HTTP_400_BAD_REQUEST)

        elif request.user.role == 300:
            if request.user.is_authentication in [0, 1]:
                return Response('律师未认证', status=status.HTTP_400_BAD_REQUEST)
            if request.user.is_authentication == 3:
                try:
                    request.user.lawyer_user
                except EnterpriseUser.DoesNotExist:
                    return Response('律师未认证', status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response('身份错误', status=status.HTTP_403_FORBIDDEN)

        with cache.lock(f'conversation_status_{pk}', timeout=10, blocking_timeout=10):
            serializer = self.get_serializer(instance, data=request.data)
            serializer.is_valid(raise_exception=True)
            another_user = serializer.validated_data.get('another_user')
            instance = self.get_object()
            if instance.status != 0:
                return Response('咨询未处于未解决状态', status=status.HTTP_400_BAD_REQUEST)
            if request.user.role in [100, 200] and not another_user:
                return Response('缺少律师参数', status=status.HTTP_400_BAD_REQUEST)

            data = {
                'type': 200,
                'title': '咨询订单',
                'conversation': instance.id,
                'conversation_type': instance.type.name,
                'content': instance.content
            }
            user = request.user

            if user.role in [100, 200]:
                user_query_param = {'ordinary_user': user}
                another_user_param = {'lawyer': another_user}
                user_len = 3
                another_user_len = 5
            elif user.role == 300:
                user_query_param = {'lawyer': user}
                another_user_param = {'ordinary_user': another_user}
                user_len = 5
                another_user_len = 3

            user_conversation_count = Conversation.objects.filter(Q(**user_query_param), Q(is_deleted=False),
                                               Q(status=1), ~Q(pk=pk))[:user_len].count()

            another_user_conversation_count = Conversation.objects.filter(Q(**another_user_param), Q(is_deleted=False),
                                               Q(status=1), ~Q(pk=pk))[:another_user_len].count()

            if user.role in [100, 200]:
                if user_conversation_count == user_len:
                    return Response('您当前咨询数量已超过上限', status=status.HTTP_400_BAD_REQUEST)
                if another_user_conversation_count == another_user_len:
                    return Response('律师的咨询数量已超过上限', status=status.HTTP_400_BAD_REQUEST)
                if Conversation.objects.filter(ordinary_user=request.user, is_deleted=False, status=1,
                                                lawyer=another_user).exists():
                    return Response('已存在与该律师的咨询', status=status.HTTP_400_BAD_REQUEST)

                instance.status = 1
                instance.lawyer = another_user
                instance.open_time = timezone.now()
                instance.save()
                post_im_message(user.uid, another_user.uid, data)

                return Response('开始咨询')
            elif user.role == 300:
                if user_conversation_count == user_len:
                    return Response('您的咨询数量已超过上限', status=status.HTTP_400_BAD_REQUEST)
                if another_user_conversation_count == another_user_len:
                    return Response('该用户的咨询数量已超过上限', status=status.HTTP_400_BAD_REQUEST)
                if Conversation.objects.filter(ordinary_user=another_user, is_deleted=False, status=1,
                                               lawyer=request.user).exists():
                    return Response('已存在与该用户的咨询', status=status.HTTP_400_BAD_REQUEST)

                instance.status = 1
                instance.lawyer = user
                instance.open_time = timezone.now()
                instance.save()
                post_im_message(user.uid, instance.ordinary_user.uid, data)

                return Response('开始咨询')

        return Response('开始失败', status=status.HTTP_400_BAD_REQUEST)

    @action(methods=['PUT'], detail=True, url_path='close',
            serializer_class=NoneSerializer)
    def close_conversation(self, request, pk, *args, **kwargs):
        '''
        结束咨询
        '''
        instance = self.get_object()

        if request.user not in [instance.lawyer, instance.ordinary_user]:
            return Response('权限错误', status=status.HTTP_403_FORBIDDEN)

        with cache.lock(f'conversation_status_{pk}', timeout=10, blocking_timeout=10):

            if instance.status != 1:
                return Response('咨询未处于进行中状态', status=status.HTTP_400_BAD_REQUEST)

            data = {
                'type': 201,
                'title': '咨询订单',
                'conversation': instance.id,
                'conversation_type': instance.type.name,
                'content': instance.content
            }

            user = request.user
            if user.role in [100, 200] and instance.ordinary_user == user:
                instance.status = 3
                instance.close_time = datetime.datetime.now()
                instance.save()
                post_im_message(user.uid, instance.lawyer.uid, data)

                # 把结束的并有支付账单的会话加入zset
                if instance.conversation_pay.filter(is_deleted=False, status=1).exists():
                    con = get_redis_connection('default')
                    mapping = {instance.pk: datetime.datetime.timestamp(instance.close_time)}
                    con.zadd('close_conversation', mapping=mapping)

                return Response('结束咨询')
            elif user.role == 300 and instance.lawyer == user:
                instance.status = 3
                instance.close_time = datetime.datetime.now()
                instance.save()
                post_im_message(user.uid, instance.ordinary_user.uid, data)

                # 把结束的并有支付账单的会话加入zset
                if instance.conversation_pay.filter(is_deleted=False, status=1).exists():
                    con = get_redis_connection('default')
                    mapping = {instance.pk: datetime.datetime.timestamp(instance.close_time)}
                    con.zadd('close_conversation', mapping=mapping)

                return Response('结束咨询')

        return Response('结束失败', status=status.HTTP_400_BAD_REQUEST)

    @action(methods=['PUT'], detail=True, url_path='cancel',
            serializer_class=NoneSerializer)
    def cancel_conversation(self, request, pk, *args, **kwargs):
        '''
        取消咨询
        '''
        instance = self.get_object()
        if request.user not in [instance.lawyer, instance.ordinary_user]:
            return Response('权限错误', status=status.HTTP_403_FORBIDDEN)

        with cache.lock(f'conversation_status_{pk}', timeout=10, blocking_timeout=10):

            if instance.status != 0:
                return Response('咨询未处于未解决状态', status=status.HTTP_400_BAD_REQUEST)

            user = request.user
            if user.role in [100, 200] and instance.ordinary_user == user:
                instance.status = 2
                instance.save()
                return Response('取消咨询')
            elif user.role == 300 and instance.lawyer == user:
                instance.status = 2
                instance.save()
                return Response('取消咨询')

        return Response('取消失败', status=status.HTTP_400_BAD_REQUEST)

    @action(methods=['GET'], detail=False, url_path='recent_conversation',
            serializer_class=RecentConversationListSerializer)
    def get_recent_conversation(self, request, *args, **kwargs):
        '''
        根据律师和用户的uid获取最近一次的咨询，参数1:another_uid(必传，律师或者用户的uid)，均为路径上拼接的参数
        '''
        another_uid = request.query_params.get('another_uid')
        if another_uid is None:
            return Response('缺少参数', status=status.HTTP_400_BAD_REQUEST)
        another_user = Users.objects.filter(uid=another_uid).first()
        if not another_user:
            con = get_redis_connection('default')
            customers = con.lrange('manager', 0, -1)
            customers = [i.decode() for i in customers]
            if another_uid not in customers:
                return Response('用户不存在', status=status.HTTP_404_NOT_FOUND)
            fields = ('id', 'type', 'status', 'create_time', 'content', 'lawyer', 'ordinary_user', 'unique_id',
                      'open_time')
            response_data = {i: {} if i in ('id', 'lawyer', 'ordinary_user') else None for i in fields}
            response_data['id'] = 0
            response_data['is_customer'] = True
            return Response(response_data)
        if another_user.role in [100, 200] and request.user.role == 300:
            instance = Conversation.objects.filter(ordinary_user=another_user, lawyer=request.user,
                                                   is_deleted=False).order_by('-update_time').first()
        elif another_user.role == 300 and request.user.role in [100, 200]:
            instance = Conversation.objects.filter(ordinary_user=request.user, lawyer=another_user,
                                                   is_deleted=False).order_by('-update_time').first()
        else:
            return Response('用户权限错误', status=status.HTTP_403_FORBIDDEN)

        if not instance:
            return Response('会话不存在', status=status.HTTP_404_NOT_FOUND)

        serializer = self.get_serializer(instance)
        data = serializer.data
        data['is_customer'] = False
        return Response(data)

    @action(methods=['GET'], detail=True, url_path='visit', permission_classes=[IsAuthenticated], url_name='visit')
    def conversation_detail(self, request, pk, *args, **kwargs):
        '''
        游客获取订单详情
        '''
        # 权限暂时改为需要认证，原因忘了
        instance = self.get_object()
        # if request.user not in [instance.lawyer, instance.ordinary_user]:
        #     return Response('权限错误', status=status.HTTP_403_FORBIDDEN)
        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    @action(methods=['GET'], detail=True, url_path='basic', url_name='conversation_basic',
            serializer_class=ConversationBasicSerializer)
    def conversation_basic_info(self, request, pk, *args, **kwargs):
        '''
        获取咨询的基本信息
        '''
        instance = self.get_object()
        if request.user not in [instance.lawyer, instance.ordinary_user]:
            return Response('权限错误', status=status.HTTP_403_FORBIDDEN)
        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    @action(methods=['GET'], detail=True, url_path='check_notice', url_name='check_notice',
            serializer_class=ConversationBasicSerializer)
    def check_notice_conversation(self, request, pk, *args, **kwargs):
        '''
        检查咨询是否需要提醒
        '''
        instance = self.get_object()
        if request.user not in [instance.lawyer, instance.ordinary_user]:
            return Response('权限错误', status=status.HTTP_403_FORBIDDEN)

        if instance.status not in [0, 1]:
            return Response({'check_notice': False})

        now = timezone.now()
        difference = (now - instance.notice_time).total_seconds()
        instance.notice_time = now
        instance.save()

        if difference >= 60 * 30:
            return Response({'check_notice': True})

        return Response({'check_notice': False})

    @transaction.atomic
    @action(methods=['POST'], detail=True, url_path='restart', url_name='restart_conversation',
            serializer_class=RestartConversationSerializer)
    def restart_conversation(self, request, pk, *args, **kwargs):
        with cache.lock(f'conversation_status_{pk}', timeout=10, blocking_timeout=10):
            instance = self.get_object()

            if instance.status not in [0, 1]:
                return Response('咨询已结束或取消')

            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)

            save_id = transaction.savepoint()

            instance.status = 3
            instance.save()

            data = {**serializer.validated_data, 'instance': instance}
            new_conversation = serializer.create(data)

            im_data = {
                'type': 200,
                'title': '咨询订单',
                'conversation': instance.id,
                'conversation_type': instance.type.name,
                'content': instance.content
            }

            if not post_im_message(request.user.uid, serializer.validated_data['lawyer'].uid, im_data):
                transaction.savepoint_rollback(save_id)

            transaction.savepoint_commit(save_id)
            return Response({'id': new_conversation.pk}, status=status.HTTP_201_CREATED)
    # @action(methods=['GET'], detail=False, url_path='recent_conversation',
    #         serializer_class=RecentConversationListSerializer)
    # def get_recent_conversation_list(self, request, *args, **kwargs):
    #     queryset = self.filter_queryset(self.get_queryset())
    #
    #     page = self.paginate_queryset(queryset)
    #     page_index = request.query_params.get('page')
    #     if page is not None and page_index is not None:
    #         serializer = self.get_serializer(page, many=True)
    #         return self.get_paginated_response(serializer.data)
    #
    #     serializer = self.get_serializer(queryset, many=True)
    #     return Response(serializer.data)


class LawyerConversationView(GenericViewSet, ListModelMixin):
    serializer_class = LawyerConversationSerializer
    queryset = Conversation.objects.filter(is_deleted=False, status=0).order_by('-id')
    filter_backends = [DjangoFilterBackend]
    filter_class = LawyerConversationSearch

    def list(self, request, *args, **kwargs):
        '''
        律师获取咨询列表，用户律师查看用户页面，city：ad_code， type：筛选是否企业用户，100为普通用户， 200为企业用户，不传为显示所有
        '''
        queryset = self.filter_queryset(self.get_queryset().filter(Q(ordinary_user__status=0),
                                                                   Q(ordinary_user__is_deleted=False),
                                                                   ~Q(ordinary_user__phone=request.user.phone)))

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response([dict(dict_flatten(i)) for i in serializer.data])

        serializer = self.get_serializer(queryset, many=True)
        return Response([dict(dict_flatten(i)) for i in serializer.data])
        # queryset = self.filter_queryset(self.get_queryset())
        # user_ids = []
        # conversation_type_ids = []
        #
        # page = self.paginate_queryset(queryset)
        # page_index = request.query_params.get('page')
        # if page is not None and page_index:
        #     serializer = self.get_serializer(page, many=True)
        #     for i, v in enumerate(serializer.data):
        #         user_ids.append(v['ordinary_user'])
        #         conversation_type_ids.append(v['type'])
        #
        #     users = Users.objects.filter(pk__in=user_ids, is_deleted=False)
        #     conversation_types = ConversationType.objects.filter(pk__in=conversation_type_ids)
        #     user_dict = {i.pk: [i, f'用户{i.phone[0:3]}****{i.phone[-4:]}'] for i in users}
        #     conversation_types_dict = {i.pk: i.name for i in conversation_types}
        #
        #     for i, v in enumerate(serializer.data):
        #         serializer.data[i]['user_phone'] = user_dict[v['ordinary_user']][1]
        #         picture = user_dict[v['ordinary_user']][0].picture
        #         serializer.data[i]['user_picture'] = f"{request.META['wsgi.url_scheme']}://{request.get_host()}{MEDIA_URL}{picture}" if picture else None
        #         serializer.data[i]['uid'] = user_dict[v['ordinary_user']][0].uid
        #         # serializer.data[i].pop('ordinary_user')
        #         serializer.data[i]['conversation_type'] = conversation_types_dict[v['type']]
        #         serializer.data[i].pop('type')
        #
        #     return self.get_paginated_response(serializer.data)
        # else:
        #     return Response('缺少分页参数', status=status.HTTP_400_BAD_REQUEST)
        # serializer = self.get_serializer(queryset, many=True)
        # return Response(serializer.data)

    @action(methods=['GET'], detail=False, url_path='visit', permission_classes=[AllowAny],
            queryset=Conversation.objects.filter(is_deleted=False).filter(~Q(status=2)).order_by('-id'))
    def get_conversation_list_with_no_login(self, request, *args, **kwargs):
        '''
            未登录获取咨询（订单）列表， 不需要Authorization
        '''
        queryset = self.filter_queryset(self.get_queryset().filter(Q(ordinary_user__status=0),
                                                                   Q(ordinary_user__is_deleted=False),))

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response([dict(dict_flatten(i)) for i in serializer.data])

        serializer = self.get_serializer(queryset, many=True)
        return Response([dict(dict_flatten(i)) for i in serializer.data])



    # @action(methods=['GET'], detail=False, url_path='user', permission_classes=[OrdinaryUserPermission],
    #         serializer_class=ConversationUserSerializer, filter_backends=(DjangoFilterBackend,),
    #         filter_class=UserConversationSearch)
    # def get_user_conversation_list(self, request, *args, **kwargs):
    #     '''
    #     获取咨询列表（我的咨询模块）,可接受传参status，不传显示所有，0表示未解决，1表示进行中， 2表示取消， 3表示已结束
    #     路径参考 /api/v1/conversation/info/user?status=0
    #     '''
    #     user = request.user
    #     if user.role in [100, 200]:
    #         queryset = self.filter_queryset(self.get_queryset().filter(ordinary_user=user).order_by('-create_time'))
    #     elif user.role == 300:
    #         queryset = self.filter_queryset(self.get_queryset().filter(lawyer=user).order_by('-create_time'))
    #     else:
    #         return Response('权限错误', status=status.HTTP_403_FORBIDDEN)
    #
    #     page = self.paginate_queryset(queryset)
    #     page_index = request.query_params.get('page')
    #     if page is not None and page_index:
    #         serializer = self.get_serializer(page, many=True)
    #         return self.get_paginated_response(serializer.data)
    #
    #     serializer = self.get_serializer(queryset, many=True)
    #     return Response({'result': serializer.data})


class ConversationTypeView(ReadOnlyModelViewSet):
    serializer_class = ConversationTypeSerializer
    queryset = ConversationType.objects.filter(is_deleted=False)
    permission_classes = [AllowAny]

    def retrieve(self, request, *args, **kwargs):
        '''
        获取单个领域, 此接口不需要token
        '''
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    def list(self, request, *args, **kwargs):
        '''
        获取所有领域列表，此接口不需要token
        '''
        queryset = self.filter_queryset(self.get_queryset())
        serializer = self.get_serializer(queryset, many=True)
        return Response({'result': serializer.data})

