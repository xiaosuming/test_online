from django.apps import AppConfig


class UsersConfig(AppConfig):
    name = 'laws_online.apps.users'
