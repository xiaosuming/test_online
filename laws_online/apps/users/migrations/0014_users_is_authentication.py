# Generated by Django 3.1.5 on 2021-01-13 19:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0013_auto_20210113_1637'),
    ]

    operations = [
        migrations.AddField(
            model_name='users',
            name='is_authentication',
            field=models.BooleanField(default=False, verbose_name='是否认证'),
        ),
    ]
