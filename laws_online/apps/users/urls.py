from django.urls import path, include
from . import views
from rest_framework import routers
from rest_framework_jwt.views import obtain_jwt_token
from rest_framework_sso.authentication import JWTAuthentication

router = routers.DefaultRouter(trailing_slash=False)
router.register(r'info', views.UserView, 'user')
router.register(r'lawyer', views.LawyerView, 'lawyer')
router.register(r'test', views.TestView, 'test')
router.register(r'enterprise', views.EnterpriseUserView, 'enterprise')
router.register(r'lawyer_case', views.LawyerCaseView, 'lawyer_case')
router.register(r'user_collect_lawyer', views.UserCollectionLawyerView, 'user_collect_lawyer')
router.register(r'enterprise_file', views.EnterpriseFileView, 'enterprise_file')
router.register(r'lawyer_file', views.LawyerFileView, 'lawyer_file')
router.register(r'wechat', views.WeixinLoginView, 'wechat_login')
router.register(r'alipay', views.AlipayLoginView, 'alipay_login')
# router.register(r'family', views.GetFamilyInfoView, 'family_info')
# router.register(r'platform', views.GetPlatformDataView, 'platform')
# router.register(r'family_information', views.FamilyInformationView, 'family_public_information')


urlpatterns = [
    path('user/', include((router.urls, 'laws_online'), namespace='user')),
    path(r'user/login/', views.LoginView.as_view(), name='login'),
]
