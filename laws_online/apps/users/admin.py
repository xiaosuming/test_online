from django.contrib import admin

# Register your models here.
from django.contrib import admin

# Register your models here.
from .models import Lawyer, Users, LawyerConversationType, LawyerFile

# Register your models here.

@admin.register(Lawyer)
class UsersAdmin(admin.ModelAdmin):
    # isdelete.short_description = '是否删除'

    list_display = ['id', 'user', 'is_deleted']
    #默认可编辑字段
    list_display_links = list_display


@admin.register(LawyerConversationType)
class LawyerConversationTypeAdmin(admin.ModelAdmin):
    # isdelete.short_description = '是否删除'

    list_display = ['id', 'is_deleted']
    #默认可编辑字段
    list_display_links = list_display


@admin.register(Users)
class UsersAdmin(admin.ModelAdmin):
    # isdelete.short_description = '是否删除'

    list_display = ['id', 'picture', 'uid', 'nickname', 'phone']
    #默认可编辑字段
    list_display_links = list_display

@admin.register(LawyerFile)
class UsersAdmin(admin.ModelAdmin):
    # isdelete.short_description = '是否删除'

    list_display = ['id', 'file', 'type']
    #默认可编辑字段
    list_display_links = list_display