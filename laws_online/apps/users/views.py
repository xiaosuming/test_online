from django.shortcuts import render, redirect
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework.viewsets import ModelViewSet, GenericViewSet, ReadOnlyModelViewSet
from rest_framework.mixins import (CreateModelMixin, RetrieveModelMixin, UpdateModelMixin, ListModelMixin,
                                   DestroyModelMixin)
from rest_framework import status
from .models import (Users, Lawyer, LawyerFile, LawyerConversationType, EnterpriseUser, LawyerCase,
                     UserCollectionLawyer, LawyerAuthenticationInfoTemp, LawyerFileTemp, EnterPriseFileTemp)
from laws_online.apps.conversation.models import ConversationType, Conversation
from .serializers import (UserSerializer, TestSerializer, MyJSONWebTokenSerializer, LawyerSerializer,
                          GetLawyerSerializer, LawyerListSerializer, GetLawyerForOrdinaryUserSerializer,
                          UpdateLawyerIsOnlineSerializer, EnterpriseCreateSerializer, EnterpriseSerializer,
                          LawyerCaseSerializer, UserCaseCollectionSerializer, UpdateUserInfoSerializer,
                          VerificationCodeSerializer, LawyerFileSerializer, CheckOldPhoneSerializer,
                          ChangePasswordWithNoLoginSerialzier, LawyerTempSerializer, GetLawyerTempSerializer,
                          EnterpriseTempSerializer, GetEnterpriseTempSerializer, UpdateEnterpriseTempSerializer,
                          UpdateLawyerTempSerializer, LawyerFileTempSerializer, EnterpriseFileTempSerializer,
                          UserChangeRoleSerializer, GetEnterpriseSerializer, WeixinLoginSerializer,
                          AlipayLoginSerializer, AlipayAuthSignSerializer)
from rest_framework.permissions import AllowAny, IsAuthenticated
from django.contrib.auth.models import AnonymousUser
from laws_online.libs.utils.tools import Response, dict_flatten
from rest_framework.response import Response as OriginResponse
from rest_framework.decorators import action
from rest_framework_jwt.views import JSONWebTokenAPIView
from .utils import get_user_sig, post_im_message_in_group, set_im_info
from laws_online.apps.users import tasks
from laws_online.libs.utils.ali_message import Sample
from django.db import transaction
from django.core.cache import cache
from django.db.models import Q
from django_filters.rest_framework import DjangoFilterBackend
from django.utils import timezone
from django.http import HttpResponseRedirect
from .filters import LawyerSearch, LawyerCaseSearch
from laws_online.apps.payment.wechatpay_tools import weixin, WeixinError
from laws_online.apps.payment.alipay_tools import get_isv_alipay, dc_alipay
from laws_online.libs.middlewares.CustomPermission import IsAuthenticatedByManager, IsAuthenticatedAndNoBanned
from laws_online.apps.conversation.serializers import ConversationTypeSerializer
import json
from rest_framework_jwt.settings import api_settings
import datetime
import random


# Create your views here.

class UserView(GenericViewSet, RetrieveModelMixin):
    serializer_class = UserSerializer
    queryset = Users.objects.filter(is_deleted=0)

    def retrieve(self, request, *args, **kwargs):
        '''
        获取个人详情
        '''
        serializer = self.get_serializer(request.user)
        # data = {**serializer.data, **{'user_sig':get_user_sig(instance.uid)}}
        return Response(serializer.data)

    @action(methods=['POST'], detail=False, url_path='register', permission_classes=[AllowAny])
    def register(self, request, *args, **kwargs):
        '''
        注册接口
        is_authentication, 0表示未提交认证， 1表示已提交未审核， 2表示已提交已审核，4表示已提交被驳回
        '''
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        phone = serializer.validated_data['phone']
        if Users.objects.filter(phone=phone, is_deleted=False).exists():
            return Response('手机号已被注册', status=status.HTTP_400_BAD_REQUEST)
        serializer.validated_data['request'] = request
        user = serializer.create(serializer.validated_data)
        if user is not None:
            data = {**UserSerializer(instance=user).data, **{'user_sig': get_user_sig(user.uid)}}
            print(data)
            return Response(data, status=status.HTTP_201_CREATED)
        return Response('用户创建失败', status=status.HTTP_400_BAD_REQUEST)

    @action(methods=['GET'], detail=False, url_path='user_sig', serializer_class=None)
    def get_user_sig_view(self, request, *args, **kwargs):
        '''
        获取用户得user_sig
        '''
        return Response({'user_sig': get_user_sig(request.user.uid)})

    @transaction.atomic
    @action(methods=['PATCH', 'PUT'], detail=False, url_path='basic_info', serializer_class=UpdateUserInfoSerializer)
    def update_basic_information(self, request, *args, **kwargs):
        '''
        部分更新用户基本信息，字段均为非必传， 更新手机号时验证码必传，修改完密码Authorization会失效，需要重新登录
        '''
        partial = kwargs.pop('partial', True)
        instance = request.user
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        picture = serializer.validated_data.get('picture')
        instance = serializer.update(instance, serializer.validated_data)
        set_im_info(instance.uid, nickname=instance.nickname) if picture is not None else None

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)


    @action(methods=['POST'], detail=False, url_path='verification_code', serializer_class=VerificationCodeSerializer,
            permission_classes=[AllowAny])
    def get_verification_code(self, request, *args, **kwargs):
        '''
        获取验证码，可不传Authorization
        '''
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        phone = serializer.validated_data['phone']
        code_type = serializer.validated_data['code_type']
        user = request.user

        check = self.check_send_times(phone)
        if check is not None:
            return Response(check, status=status.HTTP_400_BAD_REQUEST)

        code = ''.join([str(random.choice(range(10))) for i in range(6)])
        if isinstance(user, AnonymousUser):
            print(f'phone_{phone}_{code_type}')
            result, message = Sample.send(phone, code_type, code)
            if message is not None:
                return Response(message, status=status.HTTP_400_BAD_REQUEST)
            cache.set(f'phone_{phone}_{code_type}', code, 60 * 10)
            # tasks.send_message.delay(phone, code_type, code)
            return Response('验证码发送成功')
        else:
            result, message = Sample.send(phone, code_type, code)
            if message is not None:
                return Response(message, status=status.HTTP_400_BAD_REQUEST)
            cache.set(f'user_{user.id}_{phone}_{code_type}', code, 60 * 10)
            # tasks.send_message.delay(phone, code_type, code)
            return Response('验证码发送成功')

    @action(methods=['POST'], detail=False, url_path='check_old_phone', serializer_class=CheckOldPhoneSerializer)
    def check_old_phone(self, request, *args, **kwargs):
        '''
        验证旧手机号
        '''
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        phone = serializer.validated_data['phone']
        user = request.user

        if user.phone != phone:
            return Response('权限错误', status=status.HTTP_403_FORBIDDEN)

        cache_verification_code = cache.get(f'user_{user.id}_{phone}_{3}')
        if cache_verification_code != serializer.validated_data['verification_code']:
            return Response('验证码错误', status=status.HTTP_400_BAD_REQUEST)

        cache.set(f'user_{user.id}_{phone}_{5}', f'{phone}', 60 * 10)
        return Response('验证成功')

    @action(methods=['POST'], detail=False, url_path='forgot_password', permission_classes=[AllowAny],
            serializer_class=ChangePasswordWithNoLoginSerialzier)
    def change_password_with_no_login(self, request, *args, **kwargs):
        '''
            未登录忘记密码
        '''
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        phone = serializer.validated_data['phone']
        user = Users.objects.filter(is_deleted=False, phone=phone).first()
        if not user:
            return Response('用户不存在', status=status.HTTP_400_BAD_REQUEST)

        serializer.update(user, serializer.validated_data)
        return Response('修改成功')

    @transaction.atomic
    @action(methods=['POST'], detail=False, url_path='change_role', serializer_class=UserChangeRoleSerializer)
    def change_role(self, request, *args, **kwargs):
        '''
        切换身份接口
        '''
        with cache.lock(f'user_{request.user.pk}', timeout=10, blocking_timeout=10):
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)

            user = request.user
            if user.is_deleted == True:
                return Response('用户不存在', status=status.HTTP_404_NOT_FOUND)

            if user.role == serializer.validated_data['role']:
                return Response('旧身份与新身份不能相同', status=status.HTTP_400_BAD_REQUEST)

            # if user.role in [100, 200]:
            #     if user.ordinary_conversation.filter(is_deleted=False, status__in=[0, 1]).exists():
            #         return Response('存在未解决的或已开始的订单，清先取消或结束', status=status.HTTP_400_BAD_REQUEST)
            # elif user.role == 300:
            #     if user.lawyer_conversation.filter(is_deleted=False, status__in=[0, 1]).exists():
            #         return Response('存在已开始的订单，清先结束', status=status.HTTP_400_BAD_REQUEST)
            #     if user.from_user_pay.filter(is_deleted=False, settlement_status=0).exists():
            #         return Response('存在未结算的账单，请等结算后可切换身份', status=status.HTTP_400_BAD_REQUEST)
            # else:
            #     return Response('用户身份错误', status=status.HTTP_403_FORBIDDEN)

            new_user = serializer.create(serializer.validated_data)

            if new_user is not None:
                login_data = serializer.get_jwt_token(new_user)
                user = login_data['user'] or request.user
                token = login_data['token']
                response_data = jwt_response_payload_handler(token, user, request)
                response = OriginResponse(response_data)
                if api_settings.JWT_AUTH_COOKIE:
                    expiration = (datetime.utcnow() +
                                  api_settings.JWT_EXPIRATION_DELTA)
                    response.set_cookie(api_settings.JWT_AUTH_COOKIE,
                                        token,
                                        expires=expiration,
                                        httponly=True)
                user.last_login = timezone.now()
                user.save()
                return response

                # data = {**UserSerializer(instance=new_user).data, **{'user_sig': get_user_sig(new_user.uid)}}
                # print(data)
                # 切换成功变更一下登录的时间戳
                # new_user.last_login = timezone.now()
                # new_user.save()
                # return Response(data, status=status.HTTP_201_CREATED)

            return Response('用户创建失败', status=status.HTTP_400_BAD_REQUEST)


    def check_send_times(self, phone):
        '''检查短信发送频率'''
        phone_times = cache.get(f'phone_times_{phone}')
        if phone_times is None:
            now = timezone.now()
            tomorrow = now + datetime.timedelta(days=1)
            tomorrow = datetime.datetime(year=tomorrow.year, month=tomorrow.month, day=tomorrow.day)
            cache.set(f'phone_times_{phone}', 1, tomorrow.timestamp() - now.timestamp())
            phone_times = 0

        if phone_times >= 100:
            return '验证码发送超过今日上限'

        try:
            cache.incr(f'phone_times_{phone}', 1)
        except ValueError as e:
            print(e)
            return self.check_send_times(phone)

    # @action(methods=['GET'], detail=False, url_path='user_sig', serializer_class=None)
    # def get_user_lawyer_list(self, request, *args, **kwargs):
    #     '''
    #     获取律师列表
    #     '''
    #
    #     return Response({'user_sig': get_user_sig(request.user.uid)})



class LawyerView(GenericViewSet, ListModelMixin):
    serializer_class = LawyerListSerializer
    queryset = Users.objects.filter(is_deleted=False, is_authentication__in=[2, 4, 5], role=300).order_by('id')
    filter_backends = [DjangoFilterBackend]
    filter_class = LawyerSearch
    permission_classes = [AllowAny]

    def list(self, request, *args, **kwargs):
        '''
        获取律师列表,page:页码,page_size:分页大小,city:城市（暂时无作用）,conversation_type:领域类型,is_online:是否在线,
        recommendation:是否为推荐律师，传值接受1, conversation_type:咨询类型id, exclude_lawyer:排除的律师id
        '''
        #  待优化
        page_index = request.query_params.get('page')
        if kwargs.pop('collect_user', False):
            queryset = self.filter_queryset(self.get_queryset().filter(
                collection_lawyer__user=request.user, collection_lawyer__is_deleted=False))
            # from django.db import connection
            # for i in connection.queries:
            #     print(i)
        else:
            if isinstance(request.user, AnonymousUser):
                queryset = self.filter_queryset(self.get_queryset().filter(status=0))
            else:
                queryset = self.filter_queryset(
                    self.get_queryset().filter(status=0).filter(~Q(phone=request.user.phone)))
        if request.query_params.get('recommendation'):
            queryset = queryset.filter(is_online=True).order_by('?')[:3]
            if not queryset:
                if isinstance(request.user, AnonymousUser):
                    queryset = self.get_queryset().filter(status=0).filter(is_online=True).order_by('?')[:3]
                else:
                    exclude_lawyer = request.query_params.get('exclude_lawyer', 0)
                    queryset = self.get_queryset().filter(Q(status=0), Q(is_online=True), ~Q(pk=exclude_lawyer),
                                                          ~Q(pk=request.user.phone)).order_by('?')[:3]
            # if isinstance(request.user, AnonymousUser):
            #     queryset = queryset.filter(is_online=True).order_by('?')[:3]
            # else:
            # # origin_queryset = queryset.filter(is_online=True).order_by('?')[:3]
            #     queryset = queryset.filter(is_online=True).order_by('?')[:3]
            # if not origin_queryset:
            #     queryset = Users.objects.filter(is_online=True).order_by('?')[:3]

        # q = queryset.values('id', 'nickname', 'lawyer_user__city', 'lawyer_user__license_period', 'is_online', 'picture')
        page = self.paginate_queryset(queryset)

        conversation_type = ConversationType.objects.filter(is_deleted=0)
        conversation_dict = {i.pk: i.name for i in conversation_type}

        if page is not None and page_index and not request.query_params.get('recommendation'):
            serializer = self.get_serializer(page, many=True)
            print(len(serializer.data))
            user_ids = [i['id'] for i in serializer.data]
            lawyer_conversation_type = LawyerConversationType.objects.filter(is_deleted=False,
                                                    lawyer__in=user_ids).values('lawyer', 'conversation_type')
            lawyer_conversation_type_dict = {i: [] for i in user_ids}
            for i in lawyer_conversation_type:
                lawyer_conversation_type_dict[i['lawyer']].append(conversation_dict[i['conversation_type']])

            data = [dict(dict_flatten(i)) for i in serializer.data]
            for index, value in enumerate(data):
                data[index]['conversation_type'] = lawyer_conversation_type_dict[value['id']]
            # from django.db import connection
            # for i in connection.queries:
            #     print(i)
            return self.get_paginated_response({'result': data})

        serializer = self.get_serializer(queryset, many=True)
        user_ids = [i['id'] for i in serializer.data]
        lawyer_conversation_type = LawyerConversationType.objects.filter(is_deleted=False, lawyer__pk__in=user_ids).values('lawyer', 'conversation_type')
        lawyer_conversation_type_dict = {i: [] for i in user_ids}
        for i in lawyer_conversation_type:
            # print(i['lawyer'])
            lawyer_conversation_type_dict[i['lawyer']].append(conversation_dict[i['conversation_type']])

        data = [dict(dict_flatten(i)) for i in serializer.data]
        for index, value in enumerate(data):
            data[index]['conversation_type'] = lawyer_conversation_type_dict[value['id']]

        return Response({'result': data})

    @action(methods=['GET'], detail=False, url_path='info', serializer_class=GetLawyerSerializer,
            queryset=Lawyer.objects.filter(user__is_deleted=False, is_deleted=False),
            permission_classes=[IsAuthenticated])
    def get_lawyer_info(self, request, *args, **kwargs):
        '''
        获取律师认证详细信息
        '''
        is_authentication = request.user.is_authentication
        if is_authentication in [1, 3]:
            lawyer_user = request.user.lawyer_temp
            if not lawyer_user:
                return Response('信息不存在', status=status.HTTP_404_NOT_FOUND)
            serializer = GetLawyerTempSerializer(lawyer_user, context={'request':request})
            return Response(dict(dict_flatten(serializer.data)))
        else:
            try:
                lawyer_user = request.user.lawyer_user
            except Lawyer.DoesNotExist:
                return Response('信息不存在', status=status.HTTP_404_NOT_FOUND)
            serializer = GetLawyerSerializer(lawyer_user, context={'request': request})
            return Response(dict(dict_flatten(serializer.data)))
        # if request.user.is_authentication == 1:
            # lawyer_user = request.user.lawyer_temp.filter(is_deleted=False).order_by('-create_time').first()
        # try:
        #     lawyer_user = request.user.lawyer_user
        # except Lawyer.DoesNotExist:
        #     return Response('信息不存在', status=status.HTTP_400_BAD_REQUEST)
        #
        # serializer = self.get_serializer(lawyer_user)
        # return Response(dict(dict_flatten(serializer.data)))

    @action(methods=['GET'], detail=True, url_path='info_with_visit',
            serializer_class=GetLawyerForOrdinaryUserSerializer,
            queryset=Users.objects.filter(is_deleted=False, role=300).filter(~Q(is_authentication__in=[0, 1, 3])),
            permission_classes=[AllowAny])
    def get_lawyer_info_with_visit(self, request, pk, *args, **kwargs):
        '''
        游客根据id获取律师信息, 可传路径参数?uid={uid}
        '''
        uid = request.query_params.get('uid')
        uid_user = self.queryset.filter(uid=uid).first() if uid is not None else None
        if not uid_user and uid:
            return Response('律师不存在', status=status.HTTP_404_NOT_FOUND)

        instance = uid_user if uid else self.get_object()
        serializer = self.get_serializer(instance)
        return Response(dict(dict_flatten(serializer.data)))

    @transaction.atomic
    @action(methods=['POST'], detail=False, url_path='authenticate', serializer_class=LawyerTempSerializer,
            queryset=Lawyer.objects.filter(is_deleted=False), permission_classes=[IsAuthenticatedAndNoBanned])
    def create_lawyer_info(self, request, *args, **kwargs):
        '''
        创建律师认证信息
        '''
        with cache.lock(f'lawyer_{request.user.pk}', timeout=10, blocking_timeout=10):
            if request.user.role != 300:
                return Response('权限错误', status=status.HTTP_403_FORBIDDEN)
            if request.user.is_authentication != 0:
                return Response('用户已创建认证', status=status.HTTP_400_BAD_REQUEST)

            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            serializer.validated_data['user'] = request.user
            serializer.validated_data['request'] = request
            if not serializer.create(serializer.validated_data):
                return Response("创建失败", status=status.HTTP_400_BAD_REQUEST)

            im_data = {
                'type': request.user.role,
                'title': '律师认证',
                'instance': request.user.pk,
            }
            post_im_message_in_group('LawyerAuthentication', im_data, 'custom')
            return Response("创建成功", status=status.HTTP_200_OK)

    @transaction.atomic
    @action(methods=['PATCH', 'PUT'], detail=False, url_path='update', serializer_class=UpdateLawyerTempSerializer,
            queryset=LawyerAuthenticationInfoTemp.objects.filter(is_deleted=False),
            permission_classes=[IsAuthenticatedAndNoBanned])
    def update_lawyer_info(self, request, *args, **kwargs):
        '''
        更新律师认证信息
        '''
        with cache.lock(f'lawyer_{request.user.pk}', timeout=10, blocking_timeout=10):
            if request.user.role != 300:
                return Response('权限错误', status=status.HTTP_403_FORBIDDEN)
            if request.user.is_authentication == 0:
                return Response('用户未提交认证信息', status=status.HTTP_400_BAD_REQUEST)

            # instance = request.user.lawyer_temp.filter(is_deleted=False).order_by('-create_time').first()
            instance = request.user.lawyer_temp
            if instance is None:
                return Response('用户信息不存在')

            serializer = self.get_serializer(instance=instance, data=request.data, partial=True)
            serializer.is_valid(raise_exception=True)
            serializer.validated_data['user'] = request.user
            serializer.validated_data['request'] = request
            if not serializer.update(instance, serializer.validated_data):
                return Response("更新失败", status=status.HTTP_400_BAD_REQUEST)

            im_data = {
                'type': request.user.role,
                'title': '律师认证',
                'instance': request.user.pk,
            }
            post_im_message_in_group('LawyerAuthentication', im_data, 'custom')
            return Response("创建成功", status=status.HTTP_201_CREATED)

    @action(methods=['PUT'], detail=False, url_path='is_online', serializer_class=UpdateLawyerIsOnlineSerializer,
            queryset=Users.objects.filter(is_deleted=False), permission_classes=[IsAuthenticated])
    def update_is_online(self, request, *args, **kwargs):
        '''
        切换律师在线状态
        '''
        instance = request.user
        if instance.status != 0 :
            return Response('你已被禁用，无法上线', status=status.HTTP_403_FORBIDDEN)

        serializer = self.get_serializer(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response('更新成功')

    @action(methods=['GET'], detail=True, url_path='conversation_types', serializer_class=ConversationTypeSerializer,
            queryset=Users.objects.filter(is_deleted=0, role=300), permission_classes=[IsAuthenticated])
    def get_conversation_types(self, request, pk, *args, **kwargs):
        '''
        根据id获取律师的领域类型
        '''
        instance = self.get_object()
        conversation_types = ConversationType.objects.filter(is_deleted=False,
                                                             lawyer_conversation_type__lawyer=instance)
        serializer = self.get_serializer(conversation_types, many=True)
        return Response({'result': serializer.data})

    @action(methods=['GET'], detail=False, url_path='check_exist_conversation',
            queryset=Users.objects.filter(is_deleted=0, role=300), permission_classes=[IsAuthenticated])
    def get_check_exist_conversation(self, request, *args, **kwargs):
        '''
        检查是否存在未解决的会话
        '''
        user = request.user
        exist_conversation = Conversation.objects.filter(is_deleted=False, ordinary_user=user, status=0).exists()
        return Response({'result': exist_conversation})

    @action(methods=['GET'], detail=False, url_path='user_collect', permission_classes=[IsAuthenticated])
    def user_collect_lawyer(self, request, *args, **kwargs):
        '''
        获取收藏的律师
        '''
        kwargs['collect_user'] = True
        return self.list(request, *args, **kwargs)


class LawyerFileView(GenericViewSet, CreateModelMixin, DestroyModelMixin):
    serializer_class = LawyerFileTempSerializer
    queryset = LawyerFileTemp.objects.filter(is_deleted=False)

    def create(self, request, *args, **kwargs):
        '''
        创建律师认证文件
        '''
        user = request.user
        if request.user.role != 300:
            return Response('权限错误', status=status.HTTP_403_FORBIDDEN)
        if request.user.is_authentication == 0:
            return Response('用户未提交认证信息', status=status.HTTP_400_BAD_REQUEST)

        if user.lawyer_temp is None:
            return Response('认证信息不存在')

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data
        data['is_authentication'] = 0  # 新创建的都是未认证，标记为0
        data['lawyer'] = user
        instance = serializer.create(data)
        # user_authentication = user.is_authentication
        # user.is_authentication = 4 if user.is_authentication in [2, 4, 5] else 1
        # user.save() if user_authentication != user.is_authentication else None
        return Response({'id': instance.pk}, status=status.HTTP_201_CREATED)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if request.user != instance.lawyer:
            return Response('权限错误', status=status.HTTP_403_FORBIDDEN)
        instance.is_deleted = True
        # instance.
        instance.save()
        return Response('', status=status.HTTP_200_OK)



class EnterpriseUserView(GenericViewSet, RetrieveModelMixin):
    serializer_class = EnterpriseSerializer
    queryset = Users.objects.filter(is_deleted=False, ).order_by('id')
    permission_classes = [IsAuthenticated]

    def retrieve(self, request, *args, **kwargs):
        instance = request.user
        if instance.role != 200:
            return Response('权限错误', status=status.HTTP_403_FORBIDDEN)
        if instance.is_authentication in [1, 3]:
            if instance.enterprise_user_temp is None:
                return Response('认证信息不存在', status=status.HTTP_404_NOT_FOUND)

            serializer = GetEnterpriseTempSerializer(instance.enterprise_user_temp, context={'request': request})
            return Response(serializer.data)
        else:
            try:
                enterprise_user = instance.enterprise_user
            except EnterpriseUser.DoesNotExist:
                return Response('认证信息不存在', status=status.HTTP_404_NOT_FOUND)

            serializer = GetEnterpriseSerializer(enterprise_user, context={'request': request})
            return Response(serializer.data)
        # if instance.enterprise_user is None:
        #     return Response('认证信息不存在', status=status.HTTP_400_BAD_REQUEST)
        # #is_authentication=2, role=200
        #
        # serializer = GetEnterpriseTempSerializer(instance.enterprise_user, context={'request': request})
        # return Response(serializer.data)


    @transaction.atomic
    @action(methods=['POST'], detail=False, url_path='authenticate', serializer_class=EnterpriseTempSerializer,
            queryset=EnterpriseUser.objects.filter(is_deleted=False), permission_classes=[IsAuthenticatedAndNoBanned])
    def create_enterprise_info(self, request, *args, **kwargs):
        '''
        创建企业认证信息
        '''
        with cache.lock(f'enterprise_{request.user.pk}', timeout=10, blocking_timeout=10):
            if request.user.role != 200:
                return Response('权限错误', status=status.HTTP_403_FORBIDDEN)
            if request.user.is_authentication != 0:
                return Response('用户已创建认证', status=status.HTTP_400_BAD_REQUEST)

            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            serializer.validated_data['user'] = request.user
            # serializer.validated_data['request'] = request
            if not serializer.create(serializer.validated_data):
                return Response("创建失败", status=status.HTTP_400_BAD_REQUEST)

            im_data = {
                'type': request.user.role,
                'title': '企业认证',
                'instance': request.user.pk,
            }
            post_im_message_in_group('EnterpriseAuthentication', im_data, 'custom')
            return Response("创建成功", status=status.HTTP_201_CREATED)

    @transaction.atomic
    @action(methods=['PATCH', 'PUT'], detail=False, url_path='update', serializer_class=UpdateEnterpriseTempSerializer,
            queryset=LawyerAuthenticationInfoTemp.objects.filter(is_deleted=False),
            permission_classes=[IsAuthenticatedAndNoBanned])
    def update_enterprise_info(self, request, *args, **kwargs):
        '''
        更新律师认证信息
        '''
        with cache.lock(f'enterprise_{request.user.pk}', timeout=10, blocking_timeout=10):
            if request.user.role != 200:
                return Response('权限错误', status=status.HTTP_403_FORBIDDEN)
            if request.user.is_authentication == 0:
                return Response('用户未提交认证信息', status=status.HTTP_400_BAD_REQUEST)

            # instance = request.user.lawyer_temp.filter(is_deleted=False).order_by('-create_time').first()
            instance = request.user.enterprise_user_temp
            if instance is None:
                return Response('用户信息不存在')

            serializer = self.get_serializer(instance=instance, data=request.data, partial=True)
            serializer.is_valid(raise_exception=True)
            serializer.validated_data['user'] = request.user
            serializer.validated_data['request'] = request
            if not serializer.update(instance, serializer.validated_data):
                return Response("更新失败", status=status.HTTP_400_BAD_REQUEST)

            im_data = {
                'type': request.user.role,
                'title': '企业认证',
                'instance': request.user.pk,
            }
            post_im_message_in_group('EnterpriseAuthentication', im_data, 'custom')
            return Response("创建成功", status=status.HTTP_200_OK)


class EnterpriseFileView(GenericViewSet, CreateModelMixin, DestroyModelMixin):
    serializer_class = EnterpriseFileTempSerializer
    queryset = EnterPriseFileTemp.objects.filter(is_deleted=False)

    def create(self, request, *args, **kwargs):
        '''
        创建企业认证文件
        '''
        user = request.user
        if request.user.role != 200:
            return Response('权限错误', status=status.HTTP_403_FORBIDDEN)
        if request.user.is_authentication == 0:
            return Response('用户未提交认证信息', status=status.HTTP_400_BAD_REQUEST)

        if user.enterprise_user_temp is None:
            return Response('认证信息不存在')

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data
        data['is_authentication'] = 0  # 新创建的都是未认证，标记为0
        data['user'] = user
        instance = serializer.create(data)
        # user_authentication = user.is_authentication
        # user.is_authentication = 4 if user.is_authentication in [2, 4, 5] else 1
        # user.save() if user_authentication != user.is_authentication else None
        return Response({'id': instance.pk}, status=status.HTTP_201_CREATED)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if request.user != instance.lawyer:
            return Response('权限错误', status=status.HTTP_403_FORBIDDEN)
        instance.is_deleted = True
        # instance.
        instance.save()
        return Response('', status=status.HTTP_200_OK)



jwt_response_payload_handler = api_settings.JWT_RESPONSE_PAYLOAD_HANDLER

class LoginView(JSONWebTokenAPIView):
    serializer_class = MyJSONWebTokenSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            user = serializer.object.get('user') or request.user
            token = serializer.object.get('token')
            response_data = jwt_response_payload_handler(token, user, request)
            response = OriginResponse(response_data)
            if api_settings.JWT_AUTH_COOKIE:
                expiration = (datetime.utcnow() +
                              api_settings.JWT_EXPIRATION_DELTA)
                response.set_cookie(api_settings.JWT_AUTH_COOKIE,
                                    token,
                                    expires=expiration,
                                    httponly=True)
            user.last_login = timezone.now()
            user.save()
            return response
        print(serializer.errors)
        print(serializer.errors.items())
        try:
            msg = ','.join([','.join([j for j in v]) for k, v in serializer.errors.items()])
            return Response(msg, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            print(e)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LawyerCaseView(ModelViewSet):
    serializer_class = LawyerCaseSerializer
    queryset = LawyerCase.objects.filter(is_deleted=False)
    filter_backends = [DjangoFilterBackend]
    filter_class = LawyerCaseSearch

    def retrieve(self, request, *args, **kwargs):
        '''
            获取单个律师案例, 无需Authorization请使用/api/v1/user/lawyer_case/{id}/for_visit
        '''
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    def list(self, request, *args, **kwargs):
        '''
            获取律师案例列表，无需Authorization请使用/api/v1/user/lawyer_case/list/for_visit
        '''
        if request.query_params.get('user') is None:
            return Response('参数错误', status=status.HTTP_400_BAD_REQUEST)

        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


    def create(self, request, *args, **kwargs):
        '''
            创建律师案例
        '''
        if request.user.role != 300:
            return Response('权限错误', status=status.HTTP_403_FORBIDDEN)
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.validated_data.update({'user': request.user})
        instance = serializer.create(serializer.validated_data)
        headers = self.get_success_headers(serializer.data)
        return Response({'id': instance.pk}, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        '''
            更新律师案例
        '''
        if request.user.role != 300:
            return Response('权限错误', status=status.HTTP_403_FORBIDDEN)

        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        if instance.user != request.user:
            return Response('权限错误', status=status.HTTP_403_FORBIDDEN)

        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        return Response('update successfully')

    def partial_update(self, request, *args, **kwargs):
        '''
            部分更新律师案例
        '''
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        '''
        删除案例
        '''
        instance = self.get_object()
        instance.is_deleted = True
        instance.save()
        return Response('')

    @action(methods=['GET'], detail=False, url_path='list/for_visit', permission_classes=[AllowAny])
    def get_case_list_for_visit(self, request, *args, **kwargs):
        '''
            游客获取律师案例列表, 需要传路径参数user（律师的id），此参数为必传参数
        '''
        return self.list(request, *args, **kwargs)

    @action(methods=['GET'], detail=True, url_path='for_visit', permission_classes=[AllowAny])
    def get_case_for_visit(self, request, pk, *args, **kwargs):
        '''
            游客获取单个律师案例
        '''
        kwargs['pk'] = pk
        return self.retrieve(request, *args, **kwargs)


class UserCollectionLawyerView(GenericViewSet, CreateModelMixin):
    serializer_class = UserCaseCollectionSerializer
    queryset = UserCollectionLawyer.objects.filter(is_deleted=False).order_by('create_time')

    def create(self, request, *args, **kwargs):
        '''
        收藏律师
        '''
        with cache.lock(f'user_collect_lawyer_{request.user.pk}', timeout=10, blocking_timeout=10):
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            if self.queryset.filter(lawyer=serializer.validated_data['lawyer'], user=request.user,
                                    is_deleted=False).exists():
                return Response('已收藏，请勿重复收藏', status=status.HTTP_400_BAD_REQUEST)
            data = {**serializer.validated_data, **{'user': request.user}}
            serializer.create(data)
            headers = self.get_success_headers(serializer.data)
            return Response('success', status=status.HTTP_201_CREATED, headers=headers)

    @action(methods=['DELETE'], detail=False, url_path='cancel')
    def cancel_collect(self, request, *args, **kwargs):
        '''
        取消收藏,传参与上面的/api/v1/user/user_collect_lawyer一致
        '''
        with cache.lock(f'user_collect_lawyer_{request.user.pk}', timeout=10, blocking_timeout=10):
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            instance = self.queryset.filter(lawyer=serializer.validated_data['lawyer'], user=request.user,
                                    is_deleted=False).first()
            if not instance:
                return Response('收藏不存在', status=status.HTTP_400_BAD_REQUEST)
            instance.is_deleted = True
            instance.save()
            return Response('success', status=status.HTTP_200_OK)


class WeixinLoginView(GenericViewSet):
    serializer_class = WeixinLoginSerializer
    queryset = Users.objects.filter(is_deleted=False)

    @action(methods=['POST'], detail=False, url_path='get_openid')
    def get_openid(self, request, *args, **kwargs):
        '''
        获取微信的open_id
        '''
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        try:
            result = weixin.access_token(serializer.validated_data['code'])
        except WeixinError as error:
            print(error)
            return Response('授权失败', status=status.HTTP_400_BAD_REQUEST)

        return Response({'openid': result['openid']})


class AlipayLoginView(GenericViewSet):
    serializer_class = AlipayLoginSerializer
    queryset = Users.objects.filter(is_deleted=False)

    @action(methods=['POST'], detail=False, url_path='get_userid')
    def get_userid(self, request, *args, **kwargs):
        '''
        获取支付宝的user_id
        '''
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        isv_alipay = get_isv_alipay(serializer.validated_data['code'])
        result = isv_alipay.api_alipay_open_auth_token_app()
        if result.get('code') != '10000':
            print(result)
            return Response('授权失败', status=status.HTTP_400_BAD_REQUEST)

        return Response({'user_id': result['user_id']})

    @action(methods=['POST'], detail=False, url_path='sign_data', serializer_class=AlipayAuthSignSerializer)
    def sign_data(self, request, *args, **kwargs):
        '''
        获取支付宝签名
        '''
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response({'sign': dc_alipay.sign_data(serializer.validated_data)})


from asgiref.sync import sync_to_async, async_to_sync
import asyncio
from rest_framework.reverse import reverse
# from django.urls import reverse

class TestView(GenericViewSet, CreateModelMixin, RetrieveModelMixin):
    serializer_class = TestSerializer
    queryset = Users.objects.filter(is_deleted=False)
    # permission_classes = [AllowAny]
    # permission_classes = [IsAuthenticatedByManager]

    def create(self, request, *args, **kwargs):
        print(request.data)
        data = request.data
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        print(serializer.validated_data)
        r = serializer.create(serializer.validated_data)
        print(r)
        p1 = data.get('p1')
        print(p1)
        login_data = serializer.create(serializer.validated_data)
        user = login_data['user'] or request.user
        token = login_data['token']
        response_data = jwt_response_payload_handler(token, user, request)
        response = OriginResponse(response_data)
        if api_settings.JWT_AUTH_COOKIE:
            expiration = (datetime.utcnow() +
                          api_settings.JWT_EXPIRATION_DELTA)
            response.set_cookie(api_settings.JWT_AUTH_COOKIE,
                                token,
                                expires=expiration,
                                httponly=True)
        user.last_login = timezone.now()
        user.save()
        return response
        return Response(1)


    def retrieve(self, request, *args, **kwargs):
        return Response(reverse('user:user-detail' ,kwargs={'pk': 1}, request=request))


    # async def retrieve(self, request, *args, **kwargs):
    #     loop = asyncio.get_event_loop()
    #     async_function = sync_to_async(self._get_sleep())
    #     loop.create_task(async_function())
    #     return Response(1)


    def _get_sleep(self):
        import time
        time.sleep(10)
        return Response(2)

    # def update(self, request, *args, **kwargs):
    #     partial = kwargs.pop('partial', False)
    #     instance = self.get_object()
    #     serializer = TestSerializer(instance, data=request.data, partial=partial)
    #     serializer.is_valid(raise_exception=True)
    #     self.perform_update(serializer)

    # if getattr(instance, '_prefetched_objects_cache', None):
    #     # If 'prefetch_related' has been applied to a queryset, we need to
    #     # forcibly invalidate the prefetch cache on the instance.
    #     instance._prefetched_objects_cache = {}
    #
    # return Response(serializer.data)

    # @action(methods=['PUT', 'PATCH'], detail=False, url_path='update', serializer_class=TestSerializer)
    # def update_user(self, request, *args, **kwargs):
    #     serializer = self.get_serializer(data=request.data)
    #     serializer.is_valid(raise_exception=True)
    #     return Response(1)

    # @action(methods=['POST'], detail=False, url_path='test', serializer_class=TestSerializer)
    # def test(self, request, *args, **kwargs):
    #     print(request.data)
    #     print(type(request.data))
    #     u = UserSerializer(instance=request.user)
    #     print(u.data)
    #     print(UserSerializer(data=request.user))
    #     return Response(UserSerializer(instance=request.user).data)


