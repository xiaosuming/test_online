from rest_framework import serializers
from .models import Text, TextTemplateFields, UserText
from docx import Document
from rest_framework.serializers import ValidationError
import re


class TextSerializer(serializers.ModelSerializer):
    origin_file = serializers.FileField(write_only=True)
    file = serializers.FileField(read_only=True)
    pdf_file = serializers.FileField(read_only=True)

    class Meta:
        model = Text
        fields = ('id', 'name', 'file', 'origin_file', 'pdf_file')

    # def validate(self, attrs):
    #     raise ValidationError('asv')

class TextTemplateFieldsSerializer(serializers.ModelSerializer):

    class Meta:
        model = TextTemplateFields
        fields = ('id', 'name', 'form_name')


# class TextFormFieldsSerializer(serializers.Serializer):
#
#     # form_name = serializers.CharField()
#


class TextTemplateFieldsValidateSerializer(serializers.Serializer):
    id = serializers.CharField(help_text='字段id')
    value = serializers.CharField(help_text='字段的值', allow_blank=True)


class UserTextTemplateSerializer(serializers.Serializer):
    text_values = TextTemplateFieldsValidateSerializer(default=[], many=True,
                                                       help_text='类型为[{"id":1,"value":""},{"id":2,"value":""}]')
    name = serializers.CharField(max_length=150, required=False, allow_blank=True, help_text='文本名称')


class UserTextSerializer(serializers.ModelSerializer):


    class Meta:
        model = UserText
        fields = ('id', 'name', 'user', 'file', 'pdf_file')


class DocxFileSerializer(serializers.Serializer):
    docx = serializers.FileField()

    def validate(self, attrs):
        if not attrs['docx'].name.endswith('.docx'):
            raise ValidationError('文件格式必须为docx')
        return attrs


class NoneSerializer(serializers.Serializer):
    pass