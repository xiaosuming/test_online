from django.urls import path, include
from . import views
from rest_framework import routers
from rest_framework_jwt.views import obtain_jwt_token
from rest_framework_sso.authentication import JWTAuthentication

router = routers.DefaultRouter(trailing_slash=False)
router.register(r'info', views.TextView, 'text_info')
router.register(r'user', views.UserTextView, 'text_user')
# router.register(r'family', views.GetFamilyInfoView, 'family_info')
# router.register(r'platform', views.GetPlatformDataView, 'platform')
# router.register(r'family_information', views.FamilyInformationView, 'family_public_information')


urlpatterns = [
    path('text/', include((router.urls, 'laws_online'), namespace='text')),
]
