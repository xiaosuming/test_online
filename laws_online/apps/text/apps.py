from django.apps import AppConfig


class TextConfig(AppConfig):
    name = 'laws_online.apps.text'
