from django.db import models
from laws_online.apps.users.models import Users

# Create your models here.


class PlatformSuggestionComplaint(models.Model):
    user = models.ForeignKey(Users, db_constraint=False, on_delete=models.DO_NOTHING,
                             related_name='suggestion_complaint')
    type = models.IntegerField(verbose_name='投诉建议类型，投诉为1，建议为2')
    content = models.TextField(blank=True, verbose_name='投诉建议内容')
    contact = models.CharField(max_length=150, verbose_name='联系方式', default='')
    status = models.IntegerField(verbose_name='处理进度，0为未处理，1为已处理', default=0)
    unique_id = models.CharField(max_length=30, default=0)
    is_deleted = models.BooleanField(default=False)
    create_time = models.DateTimeField(auto_now_add=True)
    update_time = models.DateTimeField(auto_now=True)


class UserComplaint(models.Model):
    from_user = models.ForeignKey(Users, db_constraint=False, on_delete=models.DO_NOTHING,
                                  related_name='from_user_complaint')
    to_user = models.ForeignKey(Users, db_constraint=False, on_delete=models.DO_NOTHING,
                               related_name='to_user_complaint')
    conversation = models.ForeignKey('conversation.Conversation', db_constraint=False, on_delete=models.DO_NOTHING,
                                     related_name='complaint_conversation', verbose_name='咨询id')
    type = models.IntegerField(verbose_name='投诉原因')
    content = models.CharField(blank=True, verbose_name='投诉内容', max_length=200)
    contact = models.CharField(max_length=150, verbose_name='联系方式', default='')
    status = models.IntegerField(verbose_name='处理进度，0为未处理，1为已处理', default=0)
    unique_id = models.CharField(max_length=30, default=0)
    is_deleted = models.BooleanField(default=False)
    create_time = models.DateTimeField(auto_now_add=True)
    update_time = models.DateTimeField(auto_now=True)


class UserComplaintFile(models.Model):
    complaint = models.ForeignKey(UserComplaint, db_constraint=False, on_delete=models.DO_NOTHING,
                               related_name='lawyer_complaint_file')
    file = models.FileField(upload_to='users/complaint/lawyer', null=True, verbose_name='文件')
    type = models.IntegerField(verbose_name='暂时只有1，投诉图片', default=1)
    is_deleted = models.BooleanField(default=False)
    create_time = models.DateTimeField(auto_now_add=True)
    update_time = models.DateTimeField(auto_now=True)


class LawyerEvaluation(models.Model):
    user = models.ForeignKey(Users, db_constraint=False, on_delete=models.DO_NOTHING,
                             related_name='user_evaluation', verbose_name='用户id')
    lawyer = models.ForeignKey(Users, db_constraint=False, on_delete=models.DO_NOTHING,
                               related_name='lawyer_evaluation', verbose_name='律师id')
    conversation = models.ForeignKey('conversation.Conversation', db_constraint=False, on_delete=models.DO_NOTHING,
                               related_name='evaluation_conversation', verbose_name='咨询id')
    satisfaction_degree = models.IntegerField(verbose_name='满意度')
    content = models.CharField(max_length=100, verbose_name='评论内容')
    is_deleted = models.BooleanField(default=False)
    create_time = models.DateTimeField(auto_now_add=True)
    update_time = models.DateTimeField(auto_now=True)


class OrdinaryUserEvaluation(models.Model):
    user = models.ForeignKey(Users, db_constraint=False, on_delete=models.DO_NOTHING,
                             related_name='user_evaluation_by_lawyer', verbose_name='用户id')
    lawyer = models.ForeignKey(Users, db_constraint=False, on_delete=models.DO_NOTHING,
                               related_name='lawyer_evaluation_by_lawyer', verbose_name='律师id')
    conversation = models.ForeignKey('conversation.Conversation', db_constraint=False, on_delete=models.DO_NOTHING,
                               related_name='evaluation_conversation_by_lawyer', verbose_name='咨询id')
    satisfaction_degree = models.IntegerField(verbose_name='满意度')
    content = models.CharField(max_length=100, verbose_name='评论内容')
    is_deleted = models.BooleanField(default=False)
    create_time = models.DateTimeField(auto_now_add=True)
    update_time = models.DateTimeField(auto_now=True)


LAWYER_COMPLAINT_REASON = {
    1: '律师提前结束服务',
    2: '对律师服务不满意',
    3: '律师发表不当言论',
}

ORDINARY_USER_COMPLAINT_REASON = {
    1: '用户恶意骚扰',
    2: '用户言语不文明',
    3: '其他',
}