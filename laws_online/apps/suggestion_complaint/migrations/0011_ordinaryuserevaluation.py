# Generated by Django 3.1.5 on 2021-07-05 11:40

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('conversation', '0019_conversation_open_time'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('suggestion_complaint', '0010_auto_20210303_1000'),
    ]

    operations = [
        migrations.CreateModel(
            name='OrdinaryUserEvaluation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('satisfaction_degree', models.IntegerField(verbose_name='满意度')),
                ('content', models.CharField(max_length=100, verbose_name='评论内容')),
                ('is_deleted', models.BooleanField(default=False)),
                ('create_time', models.DateTimeField(auto_now_add=True)),
                ('update_time', models.DateTimeField(auto_now=True)),
                ('conversation', models.ForeignKey(db_constraint=False, on_delete=django.db.models.deletion.DO_NOTHING, related_name='evaluation_conversation_by_lawyer', to='conversation.conversation', verbose_name='咨询id')),
                ('lawyer', models.ForeignKey(db_constraint=False, on_delete=django.db.models.deletion.DO_NOTHING, related_name='lawyer_evaluation_by_lawyer', to=settings.AUTH_USER_MODEL, verbose_name='律师id')),
                ('user', models.ForeignKey(db_constraint=False, on_delete=django.db.models.deletion.DO_NOTHING, related_name='user_evaluation_by_lawyer', to=settings.AUTH_USER_MODEL, verbose_name='用户id')),
            ],
        ),
    ]
