from django.apps import AppConfig


class SuggestionComplaintConfig(AppConfig):
    name = 'laws_online.apps.suggestion_complaint'
