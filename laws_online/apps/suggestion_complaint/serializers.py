from rest_framework import serializers
from .models import (PlatformSuggestionComplaint, UserComplaint, UserComplaintFile, LawyerEvaluation,
                     LAWYER_COMPLAINT_REASON, OrdinaryUserEvaluation, ORDINARY_USER_COMPLAINT_REASON)
from laws_online.apps.users.models import Users
from laws_online.apps.conversation.models import Conversation
from rest_framework.serializers import ValidationError
from laws_online.libs.utils.tools import get_snowflake


class LawyerComplaintFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserComplaintFile
        fields = ['file', 'type']


class LawyerComplaintSerializer(serializers.ModelSerializer):
    from_user = serializers.PrimaryKeyRelatedField(read_only=True)
    to_user = serializers.PrimaryKeyRelatedField(help_text='律师id或用户id', read_only=True)
    conversation = serializers.PrimaryKeyRelatedField(queryset=Conversation.objects.filter(is_deleted=False),
                                                      help_text='咨询id')
    file_1 = serializers.ListField(child=serializers.ImageField(), help_text='投诉图片', write_only=True, required=False)
    type = serializers.IntegerField(help_text='投诉原因，默认为1', default=1)
    complaint_file = LawyerComplaintFileSerializer(source='lawyer_complaint_file', many=True, read_only=True)
    content = serializers.CharField(max_length=200, help_text='投诉内容')
    contact = serializers.CharField(max_length=150, help_text='联系方式')
    type_reason = serializers.SerializerMethodField()

    def get_type_reason(self, obj):
        if self.context.get('request').user.role != 300:
            return LAWYER_COMPLAINT_REASON.get(obj.type) if LAWYER_COMPLAINT_REASON.get(obj.type) else '其他'
        return ORDINARY_USER_COMPLAINT_REASON.get(obj.type) if ORDINARY_USER_COMPLAINT_REASON.get(obj.type) else '其他'

    def validate(self, attrs):
        file_1 = attrs.get('file_1')
        if file_1 and len(file_1) > 3:
            raise ValidationError('图片应小于等于3张')
        if attrs['conversation'].status != 3:
            raise ValidationError('咨询状态错误')
        return attrs

    class Meta:
        model = UserComplaint
        fields = ['id', 'from_user', 'to_user', 'type', 'content', 'contact', 'create_time', 'complaint_file', 'file_1',
                  'conversation', 'type_reason']


    def create(self, validated_data):
        data = validated_data
        file_1 = data.pop('file_1', [])
        from_user = data.pop('user')
        ordinary_user = data['conversation'].ordinary_user
        lawyer = data['conversation'].lawyer
        if from_user not in [ordinary_user, lawyer] :
            raise ValidationError('用户错误')
        to_user = lawyer if from_user != lawyer else ordinary_user
        data['unique_id'] = get_snowflake()
        data['from_user'] = from_user
        data['to_user'] = to_user
        print(data)
        complaint = UserComplaint.objects.create(**data)
        if file_1:
            file = [{'type': 1, 'file': i, 'complaint': complaint} for i in file_1]
            files_model = [UserComplaintFile(**i) for i in file]
            UserComplaintFile.objects.bulk_create(files_model)
        return complaint


class UserSimpleInfoSerializer(serializers.ModelSerializer):
    phone = serializers.SerializerMethodField()

    def get_phone(self, obj):
        return f'用户{obj.phone[0:3]}****{obj.phone[-4:]}'

    class Meta:
        model = Users
        fields = ['id', 'phone', 'picture']


class LawyerEvaluationSerializer(serializers.ModelSerializer):
    user = UserSimpleInfoSerializer(read_only=True)
    lawyer = serializers.PrimaryKeyRelatedField(help_text='律师id', read_only=True)
    conversation = serializers.PrimaryKeyRelatedField(queryset=Conversation.objects.filter(is_deleted=False),
                                                      help_text='咨询id')
    satisfaction_degree = serializers.ChoiceField(choices=[i for i in range(11)],
                                                  help_text='满意度, 以整数计数，每增加1增加半星')
    content = serializers.CharField(max_length=100, help_text='评价内容')

    def validate(self, attrs):
        if attrs['conversation'].status != 3:
            raise ValidationError('咨询状态错误')
        return attrs

    class Meta:
        model = LawyerEvaluation
        fields = ['id', 'user', 'lawyer', 'conversation', 'satisfaction_degree', 'content', 'create_time']


    def create(self, validated_data):
        data = validated_data
        data['lawyer'] = data['conversation'].lawyer
        if data['user'] != data['conversation'].ordinary_user:
            raise ValidationError('用户错误')
        # data['unique_id'] = get_snowflake()
        evaluation = LawyerEvaluation.objects.create(**data)
        return evaluation


class OrdinaryUserEvaluationSerializer(serializers.ModelSerializer):
    lawyer = UserSimpleInfoSerializer(read_only=True)
    user = serializers.PrimaryKeyRelatedField(help_text='用户id', read_only=True)
    conversation = serializers.PrimaryKeyRelatedField(queryset=Conversation.objects.filter(is_deleted=False),
                                                      help_text='咨询id')
    satisfaction_degree = serializers.ChoiceField(choices=[i for i in range(11)],
                                                  help_text='满意度, 以整数计数，每增加1增加半星')
    content = serializers.CharField(max_length=100, help_text='评价内容')

    def validate(self, attrs):
        if attrs['conversation'].status != 3:
            raise ValidationError('咨询状态错误')
        return attrs

    class Meta:
        model = OrdinaryUserEvaluation
        fields = ['id', 'user', 'lawyer', 'conversation', 'satisfaction_degree', 'content', 'create_time']


    def create(self, validated_data):
        data = validated_data
        data['lawyer'] = data['conversation'].lawyer
        data['user'] = data['conversation'].ordinary_user
        if self.context.get('request').user != data['conversation'].lawyer:
            raise ValidationError(f'用户错误')
        # data['unique_id'] = get_snowflake()
        evaluation = OrdinaryUserEvaluation.objects.create(**data)
        return evaluation


class PlatformSuggestionComplaintSerializer(serializers.ModelSerializer):
    user = serializers.PrimaryKeyRelatedField(read_only=True)
    type = serializers.ChoiceField(choices=[1, 2],help_text='投诉建议类型，投诉为1，建议为2')
    content = serializers.CharField(help_text='投诉建议内容')
    contact = serializers.CharField(max_length=150, help_text='联系方式')

    class Meta:
        model = PlatformSuggestionComplaint
        fields = ['id', 'user', 'type', 'content', 'contact', 'create_time']
