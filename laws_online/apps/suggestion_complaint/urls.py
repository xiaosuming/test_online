from django.urls import path, include
from . import views
from rest_framework import routers

router = routers.DefaultRouter(trailing_slash=False)
router.register(r'user', views.UserComplaintView, 'complaint_lawyer')
router.register(r'evaluation/lawyer', views.LawyerEvaluationView, 'evaluation_lawyer')
router.register(r'evaluation/ordinary_user', views.OrdinaryUserEvaluationView, 'evaluation_ordinary_user')
router.register(r'platform', views.PlatformSuggestionComplaintView, 'platform_complaint')


urlpatterns = [
    path('complaint/', include((router.urls, 'laws_online'), namespace='suggestion_complaint')),
]
