from django.urls import path, include
from . import views
from rest_framework import routers
# from rest_framework_jwt.views import obtain_jwt_token
# from rest_framework_sso.authentication import JWTAuthentication

router = routers.DefaultRouter(trailing_slash=False)
router.register(r'info', views.CityView, 'city_info')


urlpatterns = [
    path('city/', include((router.urls, 'laws_online'), namespace='city')),
]
