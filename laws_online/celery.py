#!/usr/bin/env python3
# -*- coding:utf-8 -*-
from __future__ import absolute_import, unicode_literals
import os
from celery import Celery
from django.conf import settings

# 获取当前文件夹名，即：Django的项目名
project_name = os.path.split(os.path.abspath('.'))[-1]
project_settings = "%s.settings" % project_name

# TODO 设置django环境 wsgi中已经设置过了, 这是是不是重复设置了, 是不是可以删除
os.environ.setdefault('DJANGO_SETTINGS_MODULE',
                      f'laws_online.settings.{os.environ.get("DJANGO_CONFIGURATION", "dev")}')

app = Celery('laws_online')

app.config_from_object('django.conf:settings')  # 使用CELERY_ 作为前缀，在settings中写配置
# app.config_from_object('django.conf:settings', namespace='CELERY')

# app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)  # 发现任务文件每个app下的task.py
app.autodiscover_tasks()


@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))  #dumps its own request information